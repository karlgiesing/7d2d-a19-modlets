using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Crop growth can be configured to be affected by adverse weather conditions.
/// </summary>
/// </example>
public class CropGrowthByWeatherManager : IHarmony
{
    public void Start()
    {
        var type = GetType().ToString();
        Debug.Log("Loading Harmony Patch: " + type);
        var harmony = new Harmony(type);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    [HarmonyPatch(typeof(BlockPlantGrowing), "UpdateTick")]
    public class CropGrowthByWeatherManager_BlockPlantGrowing_UpdateTick
    {
        /// <summary>
        /// The name of the "person" who sends chat messages to the player about crop updates.
        /// This should be a key in Localization.txt.
        /// </summary>
        public const string ChatMainName = "cropChatName";

        /// <summary>
        /// The name of the block property that specifies the minimum growth temperature.
        /// </summary>
        public const string PropBlockMinGrowthTemperature = "PlantGrowing.MinGrowthTemperature";

        /// <summary>
        /// The name of the block property that specifies the maximum growth temperature.
        /// </summary>
        public const string PropBlockMaxGrowthTemperature = "PlantGrowing.MaxGrowthTemperature";

        /// <summary>
        /// The name of the block property that specifies the minimum temperature that the crop can
        /// live. If the temperature is below this, the crop will die.
        /// </summary>
        public const string PropBlockMinAliveTemperature = "PlantGrowing.MinAliveTemperature";

        /// <summary>
        /// The name of the block property that specifies the maximum temperature that the crop can
        /// live. If the temperature is above this, the crop will die.
        /// </summary>
        public const string PropBlockMaxAliveTemperature = "PlantGrowing.MaxAliveTemperature";

        /// <summary>
        /// The name of the block property that names the block to use as a replacement when the crop
        /// dies. Upon death, the crop block will be replaced with this one.
        /// </summary>
        public const string PropBlockDead = "PlantGrowing.Dead";

        /// <summary>
        /// The name of the default block to use as a replacement when the crop dies.
        /// This block is used if PropBlockDead is not specified.
        /// </summary>
        public const string DefaultBlockDead = "treeBrownGrassDiagonal";

        /// <summary>
        /// The name of the world global environment property that determines whether alerts will be
        /// sent to players when a crop's growth is stalled due to adverse weather conditions.
        /// </summary>
        public const string PropWorldGlobalGrowthAlerts = "PlantGrowing.GrowthAlerts";

        /// <summary>
        /// The name of the world global environment property that determines whether alerts will be
        /// sent to players when a crop dies due to adverse weather conditions.
        /// </summary>
        public const string PropWorldGlobalDeathAlerts = "PlantGrowing.DeathAlerts";

        public static bool Prefix(
            // used by Harmony
            BlockPlantGrowing __instance,
            ref bool __result,
            // original params
            WorldBase _world,
            int _clrIdx,
            Vector3i _blockPos,
            BlockValue _blockValue)
        {
            var biomeDefinition = ((World)_world).GetBiome(_blockPos.x, _blockPos.z);
            if (biomeDefinition.weatherPackage == null ||
                biomeDefinition.weatherPackage.param == null)
                return true; // call through to original implementation
            
            var temperature = biomeDefinition.weatherPackage.param[0];

            // Crop death
            if (__instance.Properties.Values.ContainsKey(PropBlockMinAliveTemperature))
            {
                float minTemperature = __instance.Properties.GetFloat(PropBlockMinAliveTemperature);

                if (temperature < minTemperature)
                {
                    return HandleCropDeath(
                        Localization.Get("buffElementColdName"),
                        temperature,
                        __instance,
                        ref __result,
                        _world,
                        _clrIdx,
                        _blockPos,
                        _blockValue);
                }
            }
            if (__instance.Properties.Values.ContainsKey(PropBlockMaxAliveTemperature))
            {
                float maxTemperature = __instance.Properties.GetFloat(PropBlockMaxAliveTemperature);

                if (temperature > maxTemperature)
                {
                    return HandleCropDeath(
                        Localization.Get("buffElementHotName"),
                        temperature,
                        __instance,
                        ref __result,
                        _world,
                        _clrIdx,
                        _blockPos,
                        _blockValue);
                }
            }

            // Crop growth stalling
            var isGrowing = __instance.Properties.Values.ContainsKey("PlantGrowing.Next");
            if (isGrowing && __instance.Properties.Values.ContainsKey(PropBlockMinGrowthTemperature))
            {
                float minTemperature = __instance.Properties.GetFloat(PropBlockMinGrowthTemperature);

                if (temperature < minTemperature)
                {
                    return HandleCropGrowthFailure(
                        Localization.Get("buffElementColdName"),
                        temperature,
                        __instance,
                        ref __result,
                        _world,
                        _clrIdx,
                        _blockPos);
                }
            }
            if (isGrowing && __instance.Properties.Values.ContainsKey(PropBlockMaxGrowthTemperature))
            {
                float maxTemperature = __instance.Properties.GetFloat(PropBlockMaxGrowthTemperature);

                if (temperature > maxTemperature)
                {
                    return HandleCropGrowthFailure(
                        Localization.Get("buffElementHotName"),
                        temperature,
                        __instance,
                        ref __result,
                        _world,
                        _clrIdx,
                        _blockPos);
                }
            }

            return true; // call through to original implementation
        }

        private static bool HandleCropGrowthFailure(
            string condition,
            float value,
            BlockPlantGrowing __instance,
            ref bool __result,
            WorldBase _world,
            int _clrIdx,
            Vector3i _blockPos)
        {
            var msg = string.Format(
                Localization.Get("cropNoGrowthDesc"),
                condition,
                Localization.Get(__instance.GetBlockName()));
            
            Log.Out(msg + ": " + value);

            if (WorldEnvironment.Properties.GetBool(PropWorldGlobalGrowthAlerts))
            {
                GameManager.Instance.ChatMessageServer(
                    null,
                    EChatType.Global,
                    -1,
                    msg,
                    ChatMainName,
                    true,
                    null);
            }

            // Tick rate: 63.0 (from XML) * 20f * 60f = 75,600
            // If a "tick" is 1000 per game hour, that's 3.15 game days, which is much too long to
            // wait until we check again to see if the crop will grow.
            // Dividing by 5 gives a little over 15 hours - hopefully that's good.
            ulong tickRate = __instance.GetTickRate() / 5UL;

            _world.GetWBT().AddScheduledBlockUpdate(
                _clrIdx,
                _blockPos,
                __instance.blockID,
                tickRate);
            
            // Signify to TFP code that we've handled this tick update
            __result = true;

            // Signify to Harmony that the original implementation should not be called
            return false;
        }

        private static bool HandleCropDeath(
            string condition,
            float value,
            BlockPlantGrowing __instance,
            ref bool __result,
            WorldBase _world,
            int _clrIdx,
            Vector3i _blockPos,
            BlockValue _blockValue)
        {
            var msg = string.Format(
                Localization.Get("cropDeathDesc"),
                condition,
                Localization.Get(__instance.GetBlockName()));

            Log.Out(msg + ": " + value);

            if (WorldEnvironment.Properties.GetBool(PropWorldGlobalDeathAlerts))
            {
                GameManager.Instance.ChatMessageServer(
                    null,
                    EChatType.Global,
                    -1,
                    msg,
                    ChatMainName,
                    true,
                    null);
            }

            ReplaceBlock(__instance, _world, _clrIdx, _blockPos, _blockValue);
            
            // Signify to TFP code that we've handled this tick update
            __result = true;

            // Signify to Harmony that the original implementation should not be called
            return false;
        }

        private static void ReplaceBlock(
            BlockPlantGrowing __instance,
            WorldBase _world,
            int _clrIdx,
            Vector3i _blockPos,
            BlockValue _blockValue)
        {
            var deadBlock = __instance.Properties.GetString(PropBlockDead);
            if (string.IsNullOrEmpty(deadBlock))
            {
                deadBlock = DefaultBlockDead;
            }

            var nextPlant = ItemClass.GetItem(deadBlock, false).ToBlockValue();

            _blockValue.type = nextPlant.type;

            var blockValue = BlockPlaceholderMap.Instance.Replace(
                _blockValue,
                _world.GetGameRandom(),
                _blockPos.x,
                _blockPos.z,
                false,
                QuestTags.none);

            blockValue.rotation = _blockValue.rotation;
            blockValue.meta = _blockValue.meta;
            blockValue.meta2 = 0;

            _world.SetBlockRPC(_clrIdx, _blockPos, blockValue);
        }

        public static void Postfix(
            // used by Harmony
            BlockPlantGrowing __instance,
            ref bool __result,
            // original params
            WorldBase _world,
            int _clrIdx,
            Vector3i _blockPos)
        {
            // This code is to handle cases where the updated block is fully-grown and
            // harvestable, but still may have min/max survival temperatures
            var nextPlant = _world.GetBlock(_clrIdx, _blockPos);
            var nextBlock = Block.list[nextPlant.type];
            
            if (!(nextBlock is BlockPlantGrowing))
                return;
            
            var isKillable =
                nextBlock.Properties.Values.ContainsKey(PropBlockMinAliveTemperature) ||
                nextBlock.Properties.Values.ContainsKey(PropBlockMaxAliveTemperature);
            
            // If the block has a next block, the original implementation will have already
            // scheduled a block update, so we don't have to
            var isGrowing = nextBlock.Properties.Values.ContainsKey("PlantGrowing.Next");

            if (isKillable && !isGrowing)
            {
                Log.Out("Scheduling block update for " + nextBlock.GetBlockName());

                _world.GetWBT().AddScheduledBlockUpdate(
                    _clrIdx,
                    _blockPos,
                    nextBlock.blockID,
                    nextBlock.GetTickRate());
            }
        }
    }

}
