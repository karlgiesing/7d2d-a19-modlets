# Crop Growth by Weather

A modlet that allows crop growth to be negatively affected by adverse weather conditions.

## Features

Right now, the only thing that affects crops is the biome temperature.

Each crop has a general range of temperatures in which it can grow.
If the temperatures go outside this range, crop growth is stalled.
Extreme temperatures outside this range will cause the crop to die.

Harvestable, fully-grown crops are also affected.
Extreme temperatures will also cause them to die.

If a crop dies, a message is sent to the players in the chat.
This modlet can also message players if a crop's growth is stalled,
but this feature is turned off by default.
This can be customized; see below.

If a crop grows naturally in a specific biome
(like aloe in the desert biome or blueberries in the snow biome),
that crop's temperature range is generally matched to that biome.

For crops that don't grow in a particular biome,
I have tried to follow the temperatures for that crop to grow in real life,
but with adjustments to account for the biome temperature ranges in 7D2D.

Specifically:

* Blueberries grow best in cold temperatures.
* Mushrooms and potatoes grow best in cool temperatures.
* Pumpkins and cotton grow best in warm temperatures.
* Aloe and yucca grow best in hot temperatures.
* "Super corn" grows well in nearly all temperatures.
* Everything else grows best in moderate temperatures.

As a practical matter, this means you can grow most crops in the forest, burnt forest,
or wasteland biomes, though you will probably have some crops stall or die.
But there are some crops that can realistically be grown only in the snow or desert biomes,
and you can't grow many other crops in those biomes.

This modlet should be a good match for my Seasonal Weather modlet.
You may also consider a adding modlet that introduces food spoilage,
such as SphereII's Food Spoilage modlet.

## Technical Details

**This modlet requires DMT.**
It is *not* compatible with EAC.

After installation, it is highly recommended to start a new game.

### Installation

I recommend using the Mod Launcher to install this modlet.
If it does not show up in the Mod Launcher's Mod Downloader screen,
you can just put the mod folder into the 7D2D `Mods` folder, as you would any other modlet.
So long as it is a game managed by the Mod Launcher, the program will automatically detect that
it requires DMT, and do all the necessary compilation.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/4068-the-7d2d-mod-launcher-a-mod-launcher-for-7-days-to-die/

The other option is to use the DMT tool directly.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/13037-dmt-modding-tool/

### Customizing

The messaging can be turned on or off in `worldglobal.xml`:

* `PlantGrowing.GrowthAlerts` turns on/off messages sent whenever a crop's growth is slowed
* `PlantGrowing.DeathAlerts` turns on/off messages sent whenever a crop dies

It is also possible to customize how much each crop is affected by the weather.

Each crop has four different variables that can be set, that determine how they react to the
current biome temperature.
These variables are represented by properties on the crop's block, as specified in `blocks.xml`:

* `PlantGrowing.MinGrowthTemperature`: The minimum temperature required for the crop to grow.
* `PlantGrowing.MaxGrowthTemperature`: The maximum temperature required for the crop to grow.
* `PlantGrowing.MinAliveTemperature`: The minimum temperature required for the crop to stay alive.
  If the temperature goes below this number, the growing plant dies.
* `PlantGrowing.MaxGrowthTemperature`: The maximum temperature required for the crop to stay alive.
  If the temperature goes above this number, the growing plant dies.

You can also specify the block that replaces the crop when the crop dies.
To do this, set the `PlantGrowing.Dead` property to the name of a block in `blocks.xml`.

If you do not want harvestable, fully-grown crops to die in extreme weather conditions,
comment out this XML:
```xml
<append xpath="blocks/block[contains(@name, '3HarvestPlayer')]">
    <property name="Class" value="PlantGrowing" />
    <property name="PlantGrowing.GrowthRate" value="63.0" />
</append>
```

Those crops will no longer be of the `PlantGrowing` class, and will not be affected by the weather.

### Possible Improvements

These are things that I could do in future updates, if demand exists, and time permits.

* Add other weather variables, such as the number of days since the last rainstorm,
  the amount the crop is enclosed (inside a building - see `EntityStats.GetAmountEnclosure`),
  or a flag to have crops die if it snows.
* Improved translations.
  (I translated everything using the web, and I'm sure many are terrible.)
