using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Returns human-readable information about (hired) NPCs.
/// This is meant as a stopgap until a solution is implemented in SphereII Core.
/// </summary>
public class HumanReadableNpcInfo : IHarmony
{
    public void Start()
    {
        var type = GetType().ToString();
        Debug.Log("Loading Harmony Patch: " + type);
        var harmony = new Harmony(type);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    /// <summary>
    /// Harmony postfix patch of the SphereII Core EntityAliveSDX.ToString method.
    /// </summary>
    [HarmonyPatch(typeof(EntityAliveSDX), "ToString")]
    public class RemoveFactionFriendlyFire_EntityUtilities_IsAnAlly
    {
        public static string Postfix(string __result, EntityAliveSDX __instance)
        {
            __result = "I am " + __instance.EntityName;
            var factionName = FactionManager.Instance.GetFaction(__instance.factionId).Name;
            __result += "\nof the " + factionName + " faction.";
            __result += "\nI am feeling " + GetHealthMessage(__instance) + ".";
            if (!__instance.Buffs.HasCustomVar("Owner"))
            {
                var entityClass = EntityClass.list[__instance.entityClass];
                var currency = entityClass.Properties.Values.ContainsKey("HireCurrency")
                    ? entityClass.Properties.Values["HireCurrency"]
                    : "casinoCoin";
                
                __result += "\nI will join you for "
                    + EntityUtilities.GetHireCost(__instance.entityId) + " "
                    + Localization.Get(currency);
            }
            return __result;
        }

        public static string GetHealthMessage(EntityAliveSDX __instance)
        {
            var healthiness = __instance.Health / (float) __instance.GetMaxHealth();
            
            if (healthiness > .9)
                return Localization.Get("attFortitudeRank06Desc");
            if (healthiness > .7)
                return Localization.Get("attFortitudeRank05Desc");
            if (healthiness > .5)
                return Localization.Get("attFortitudeRank04Desc");
            if (healthiness > .3)
                return Localization.Get("attFortitudeRank03Desc");
            return Localization.Get("attFortitudeRank02Desc");
        }
    }
}
