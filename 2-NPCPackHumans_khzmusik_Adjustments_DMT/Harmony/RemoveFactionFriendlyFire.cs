using DMT;
using HarmonyLib;
using System;
using System.Collections.Generic;
using System.Reflection;
using UnityEngine;

/// <summary>
/// Removes faction friendly fire.
/// </summary>
public class RemoveFactionFriendlyFire : IHarmony
{
    public void Start()
    {
        var type = GetType().ToString();
        Debug.Log("Loading Harmony Patch: " + type);
        var harmony = new Harmony(type);
        harmony.PatchAll(Assembly.GetExecutingAssembly());
    }

    /// <summary>
    /// Harmony postfix patch of the SphereII Core EntityUtilities.IsAnAlly static method.
    /// </summary>
    [HarmonyPatch(typeof(EntityUtilities), "IsAnAlly")]
    public class RemoveFactionFriendlyFire_EntityUtilities_IsAnAlly
    {
        public static bool Postfix(bool __result, int EntityID, int AttackingID)
        {
            if (!__result)
            {
                // If not already considered an ally, consider the attacker an ally if they
                // are in the same faction.
                var entity = GameManager.Instance.World.GetEntity(EntityID) as EntityAlive;
                var attacker = GameManager.Instance.World.GetEntity(AttackingID) as EntityAlive;

                __result = entity != null
                    && attacker != null
                    && entity.factionId == attacker.factionId;
            }
            return __result;
        }
    }
}
