# Adjustments to 1-NPCPackHumans

Makes numerous adjustments to `1-NPCPackHumans`.

## Features

This modlet re-implements all the features of these A18 modlets:
* `2-NPCPackHumans_Add_To_Vanilla_Spawns`
* `2-CreaturePackHumans_Remove_Harley`
* `2-CreaturePackHumans_Remove_NPC_Tracking`
* `2-NPCPackHumans_AdjustSounds`
* `Xyth NPCs_Reduce_HP`

It additionally adds these features:

* Adjusts damages for NPC weapons to be closer to PC versions
* Hire costs are calculated by weapon type and armor (see `hiring.js`) -
    this replaces the A18 `Xyth NPCs_Reduce_Hire_Costs` modlet,
    but increases the costs, rather than reducing them
* The response from "Tell me about yourself" includes faction and health information,
  and will tell you how much it costs to hire the NPC (if not already hired)
* The Power Armor vault dweller uses a stun baton instead of a club
* NPC loot bag overhaul:
    * NPC loot bags look like dropped player backpacks
    * Adds new loot lists for loot bags, specialized by weapon/armor/NPC specialty
    * Translations for all dropped loot bags
      (it's just "Dropped Loot" but better than nothing)

It also fixes these bugs (or at least I consider them bugs):

* Hostile animals now attack NPCs
* Freindly fire for NPCs of same faction is now eliminated
* Dead NPC bodies no longer have a prompt to search them (which did nothing)
* NPCs can no longer push players around

The various XML and C# files have extensive documentation about these features.
Feel free to poke around, and modify whatever you like.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `1-NPCPackHumans` modlet.
That modlet, in turn, is dependent upon:
* `0-CreaturePackHumans`
* `0-SphereIICore`
* `SphereII NPC Dialog Windows`

These dependencies are listed under the `<DMT>` section in `ModInfo.xml`.

## Technical Details

**This modlet requires DMT.**
It is *not* compatible with EAC.

Additionally, the `1-NPCPackHumans` modlet is dependent upon the `0-CreaturePackHumans` modlet.
That modlet contains new resources, which are _not_ pushed from server to client.

It is also dependent upon the `0-SphereIICore` and `SphereII NPC Dialog Windows` modlets.
Those modlets contain custom C# code, in addition to new resources,
so they are _not_ compatible with EAC either.

Both clients and servers must have copies of these modlets.
EAC must be disabled on both clients and servers.

### Installation

I recommend using the Mod Launcher to install this modlet.
If it does not show up in the Mod Launcher's Mod Downloader screen,
you can just put the mod folder into the 7D2D `Mods` folder, as you would any other modlet.
So long as it is a game managed by the Mod Launcher, the program will automatically detect that
it requires DMT, and do all the necessary compilation.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/4068-the-7d2d-mod-launcher-a-mod-launcher-for-7-days-to-die/

The other option is to use the DMT tool directly.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/13037-dmt-modding-tool/

After installation, it is highly recommended to start a new game.

### Possible Improvements

These are some ideas about possible features and bug fixes.
They require C# changes, and are not particularly easy to do.

* NPCs that spawn as part of a Clear quest, count as "zombies" to be cleared.
  You have to either kill them or fail the quest.
  This was avoided by not placing NPCs into zombie sleeper spawn groups (unfortunately).
* NPC "follow" pathing is not release worthy.
    * If you approach a following NPC to try to move them out of the way, they step into you, not away from you. 
      (They do not do this when _not_ following.)
    * NPCs do not find open paths to their leaders, instead trying to go through walls or other barriers.
      This is especially noticeable when they are above or below their leader.
      (This might be caused by SphereII Core code that lets NPCs open doors; more research is needed.)
