/**
 * @file Generates XML text for entitygroups.xml which adds the NPCPack humans to the custom
 *       entity groups created in the Factions modlet.
 *       It does so programatically, so further edits may still be required.
 */

const factions = buildFactions();
const weapons = buildWeapons();
const gs = buildGameStageStrings();

const tiers = gs.length;

buildNpcs(factions, weapons);

calculateAll(factions);

printResults(factions);

function buildFactions() {
    return {
        bandits: {
            All: [],
            Melee: [],
            Ranged: []
        },
        military: {
            All: [],
            Melee: [],
            Ranged: []
        },
        whisperers: {
            All: [],
            Melee: [],
            Ranged: []
        },
        whiteriver: {
            All: [],
            Melee: [],
            Ranged: []
        }
    }
}

function buildGameStageStrings() {
    return [
        'GS01',
        'GS50',
        'GS100',
        'GS200',
        'GS400',
        'GS800'
    ];
}

function buildNpcs(factions, weapons) {
    factions.bandits.All = weapons.flatMap(weapon => {
        return [
            {
                name: `npcEve${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcHeavyGuy${weapon.name}`,
                health: 300,
                armor: 14.94,
                weapon: weapon,
                probs: []
            }
            // These folks can't be hired
            // {
            //     name: `humanRaiderHumongous${weapon.name}`,
            //     health: 500,
            //     armor: 10.25,
            //     weapon: weapon,
            //     probs: []
            // },
            // {
            //     name: `humanRaiderMoto${weapon.name}`,
            //     health: 200,
            //     armor: 24.92,
            //     weapon: weapon,
            //     probs: []
            // },
            // {
            //     name: `humanRaiderPest${weapon.name}`,
            //     health: 200,
            //     armor: 7.47,
            //     weapon: weapon,
            //     probs: []
            // },
            // {
            //     name: `humanRaiderRat${weapon.name}`,
            //     health: 200,
            //     armor: 14.77,
            //     weapon: weapon,
            //     probs: []
            // },
            // {
            //     name: `humanRaiderSlugger${weapon.name}`,
            //     health: 300,
            //     armor: 7.47,
            //     weapon: weapon,
            //     probs: []
            // }
        ]
    })
    .filter(filterBandits)
    .filter(filterShotty)
    .sort(npcSorter);
    factions.bandits.Melee = factions.bandits.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.bandits.Ranged = factions.bandits.All.filter(e => e.weapon.isRanged).map(deepClone);

    factions.military.All = weapons.flatMap(weapon => {
        return [
            {
                name: `npcSoldierGirl01${weapon.name}`,
                health: 200,
                armor: 8.93,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcSoldierDelta${weapon.name}`,
                health: 500,
                armor: 44.65,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcSoldierNationalGuard${weapon.name}`,
                health: 500,
                armor: 44.65,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcSoldierRanger${weapon.name}`,
                health: 500,
                armor: 44.65,
                weapon: weapon,
                probs: []
            }
        ]
    })
    // Military NPCs always have weapons
    .filter(npc => npc.weapon.name != 'EmptyHand')
    .filter(filterShotty)
    .sort(npcSorter);
    factions.military.Melee = factions.military.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.military.Ranged = factions.military.All.filter(e => e.weapon.isRanged).map(deepClone);

    factions.whisperers.All = weapons.flatMap(weapon => {
        return [
            {
                name: `npcWhispererFemale01${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcWhispererFemale02${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcWhispererMale01${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcWhispererMale02${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcWhispererAlpha${weapon.name}`,
                health: 400,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcWhispererBeta${weapon.name}`,
                health: 400,
                armor: 0,
                weapon: weapon,
                probs: []
            },
        ]
    })
    .filter(filterShotty)
    .sort(npcSorter);
    factions.whisperers.Melee = factions.whisperers.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.whisperers.Ranged = factions.whisperers.All.filter(e => e.weapon.isRanged).map(deepClone);

    factions.whiteriver.All = weapons.flatMap(weapon => {
        return [
            {
                name: `npcBaker${weapon.name}`,
                health: 200,
                armor: 10.54,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcCowboy${weapon.name}`,
                health: 200,
                armor: 21.08,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcFarmer${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcNurse${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `npcSoldierHunter${weapon.name}`,
                health: 300,
                armor: 17.86,
                weapon: weapon,
                probs: []
            }
        ]
    })
    // The soldier hunter always has a weapon
    .filter(npc => !(npc.name.includes('Soldier') && npc.weapon.name == 'EmptyHand'))
    .filter(filterShotty)
    .sort(npcSorter);
    factions.whiteriver.Melee = factions.whiteriver.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.whiteriver.Ranged = factions.whiteriver.All.filter(e => e.weapon.isRanged).map(deepClone);
}

function buildWeapons() {
    return [
        {
            name: 'EmptyHand',
            dmg: 9.1,
            isRanged: false
        },
        {
            name: 'Club',
            dmg: 13.8,
            isRanged: false
        },
        {
            name: 'Knife',
            dmg: 19.8,
            isRanged: false
        },
        {
            name: 'SMG',
            dmg: 64, // doubled for multiple shots
            isRanged: true
        },
        {
            name: 'AK47',
            dmg: 100, // doubled for multiple shots
            isRanged: true
        },
        {
            name: 'Hunter',
            dmg: 50,
            isRanged: true
        },
        {
            name: 'Pistol',
            dmg: 32,
            isRanged: true
        },
        {
            name: 'XBow',
            dmg: 38,
            isRanged: true
        },
        {
            name: 'Bow',
            dmg: 28,
            isRanged: true
        },
        {
            name: 'Shotty',
            dmg: 56,  // assumes per pellet damage
            isRanged: true
        }
    ];
}

function calculateAll(factions) {
    for (var factionName in factions) {
        if (factions.hasOwnProperty(factionName)) {
            var faction = factions[factionName];
            for (var weaponType in faction) {
                if (faction.hasOwnProperty(weaponType)) {
                    calculateScores(faction[weaponType]);
                    calculateHireCosts(faction[weaponType]);
                }
            }
        }
    }
}

function calculateScores(npcs) {
    for (var i = 0; i < npcs.length; i++) {
        var weaponScore = npcs[i].weapon.dmg * (npcs[i].weapon.isRanged ? 3 : 1);
        
        npcs[i].score =
            (0.5 * npcs[i].health) +
            (3 * npcs[i].armor) +
            weaponScore;
    }
}

function calculateHireCosts(npcs) {
    for (var i = 0; i < npcs.length; i++) {
        var hireCost = npcs[i].score * 10;

        // Round to nearest 100
        npcs[i].hireCost = Math.round(hireCost / 100) * 100;
    }
}

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function filterBandits(npc) {
    // Only the HeavyGuy is a brawler
    if (npc.weapon.name == 'EmptyHand' && !npc.name.includes('HeavyGuy'))
        return false;

    // CP human raiders don't use bows
    if (npc.weapon.name == 'Bow' && npc.name.includes('human'))
        return false;

    return true;
}

function filterShotty(npc) {
    // NPCPack humans don't use shotguns
    return !(npc.name.includes('npc') && npc.weapon.name == 'Shotty');
}

function npcSorter(a, b) {
    return a.name.localeCompare(b.name);
}

function printResults(factions) {
    console.log('<configs>');
    console.log(`    <remove xpath="//entity_class[@name='npcFarmerEmptyHand']/property[@name='HireCost']" />`);
    for (var faction in factions) {
        if (factions.hasOwnProperty(faction)) {
            printFactionXml(factions[faction]);
        }
    }
    console.log('</configs>');
}

function printFactionXml(faction) {
    faction.All.forEach(npc => {
        console.log(`    <append xpath="//entity_class[@name='${npc.name}']">`);
        console.log(`        <property name="HireCost" value="${npc.hireCost}" />`);
        console.log('    </append>');
    });
}
