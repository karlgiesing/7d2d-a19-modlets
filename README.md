# 7D2D Modlets

Modlets (small modifications) for the game 7 Days To Die (aka 7D2D):
[7daystodie.com](https://7daystodie.com/)

I am user [khzmusik](https://community.7daystodie.com/profile/51265-khzmusik/) on the 7D2D forums.

## Installation

By convention, modlet creators create new repos for each alpha version of 7D2D.
These modlets were created for, and tested with, Alpha 19.

Modlets for 7D2D Alpha 18 are in this repo:
https://gitlab.com/karlgiesing/7d2d-modlets

Each modlet has an individual `README.md` file.
Under the technical details secion, it should tell you whether you need to start a new game
or generate a new world.
But if you want to be safe, generate a new game world after installing *any* modlets.

Unless the modlet name ends in "DMT", these modlets use XML and XPath,
and do not require DMT or changes to any .dll files.
They may be used with or without EAC enabled, and may be installed on any multi-player server.

However, many are dependent upon modlets created by other people.
While my modlets may not require SDX/DMT, they may be dependent upon other modlets that do.
Make sure you understand the requirements of these modlet depedencies before installing mine.
The `README.md` files should list any dependencies on other modlets.

The modlets that end in "DMT" require the modlets to be compiled with DMT.
I recommend using the Mod Launcher for this, but the DMT tool can also be used (see below).

Modlets whose name ends in "DMT" must be built and run using the DMT tool.
DMT modlets are _not_ compatible with EAC (Easy Anti-Cheat).
If you are running a multi-player game server, and want to allow DMT modlets, EAC must be off.
Most DMT modlets should be installed on both the client and the server;
see the modlet's `README.md` for details.

### Installation using the Mod Launcher

> The Mod Launcher works with both DMT and XML/XPath modlets.

Using the Mod Launcher is the recommended way to install these modlets.
In addition to being easier to work with,
it can automatically update any modlets it installs,
and will transparently build any modlets that requires DMT.

The Mod Launcher can be downloaded here:

* [7d2dmodlauncher.org](http://7d2dmodlauncher.org)

Since most of these modlets are included in the Mod Launcher database,
you can follow these instructions:

* [Managing Modlets](http://7d2dmodlauncher.org/ManagingModlets.html)

I recommend creating a new My Mods, and installing modlets into that;
this keeps the My Mods game files completely separate from the vanilla game.

To create a new My Mods:

1. In the Mod Launcher, open `File > Add New My Mods`.
1. Enter a name of your choice into the "My Server Name" field.
   This will end up being a folder name on your system.
1. Click "Save". This will save the configuration for the new My Mod, but it won't be installed.
1. Navigate to the My Mod you created. It should be under "My Mods" in the navigation pane on the left.
1. Install the new My Mod by clicking "Install Game Copy".
   It is recommended to use the "Copy from an existing copy" option.
   (Note: this will take a while, be patient.)
1. Follow the "Managing Modlets" instructions, above, to install whichever modlets you desire.
   Filter by author "khzmusik" to see only my modlets.
1. Run the game by clicking the "Play Mod" button in the Mod Launcher.

### Installation into the "vanilla" 7D2D game folder

> **XML/XPath modlets only.**
> 
> Modlets requiring DMT will need to be either installed using the Mod Launcher,
> or built using the DMT tool itself.

With this method, you will download the source code from the GitLab site,
and move the code directly into your 7D2D game folder.

1. Open the GitLab page for this project:
   
   [https://gitlab.com/karlgiesing/7d2d-a19-modlets](https://gitlab.com/karlgiesing/7d2d-a19-modlets)
1. Make sure you are on the master branch (by default, you should be).
1. On the middle right-hand side of the page, below the description, will be a download button;
   click it and select the compression type of your choice (e.g. "zip" for Windows users).
1. Download the compressed file to a directory of your choice.
1. Uncompress the file. This will create a folder called `7d2d-a19-modlets-master`.
1. Open up your 7D2D game folder. On Windows, it is usually located here:
   
   `C:\Program Files (x86)\Steam\steamapps\common\7 Days To Die`
1. Create a `Mods` folder if one does not already exist.
1. Move any desired modlets from the `7d2d-modlets-master` directory into the game's `Mods` folder.

The next time the game is started, these modlets will be active.

### Installation using DMT

> **Recommended only for advanced users.**
> 
> The DMT tool is designed for modders who are writing their own modlets.
> Though it certainly can be used as a DMT modlet manager,
> it is probably too complicated for casual users.
> If you just want to use the DMT modlets, I recommend the Mod Launcher instead.

With this method, you will download the source code from the GitLab site,
and compile the modlet(s) using the DMT tool.

The DMT tool can be found here:

> [DMT tool in the 7D2D forums](https://community.7daystodie.com/topic/13037-dmt-modding-tool/)
> 
> [GitHub repository](https://github.com/HAL-NINE-THOUSAND/DMT)

DMT's Harmony documentation, including DMT installation and configuration instructions, is here:

> [Harmony Docs](https://7d2dmods.github.io/HarmonyDocs/)

Once you have DMT installed and configured, you can use it to install any modlets requiring DMT.

1. Open the GitLab page for this project:
   
   [https://gitlab.com/karlgiesing/7d2d-a19-modlets](https://gitlab.com/karlgiesing/7d2d-a19-modlets)
1. Make sure you are on the master branch (by default, you should be).
1. On the middle right-hand side of the page, below the description, will be a download button;
   click it and select the compression type of your choice (e.g. "zip" for Windows users).
1. Download the compressed file to a directory of your choice.
1. Uncompress the file. This will create a folder called `7d2d-a19-modlets-master`.
1. Move any desired modlets from the `7d2d-modlets-master` directory into a directory of your choice.
   If you are using any other DMT modlets, put them in the same folder as the other modlets.
1. Follow the
   [Harmony Docs instructions](https://7d2dmods.github.io/HarmonyDocs/index.htm?InitialSteps.html)
   to build the modlet(s) and patch them into the game.

The next time the game is started, these modlets will be active.

### Installation into the Mod Launcher's MyMods game folder

> The Mod Launcher works with both DMT and XML/XPath modlets.

You should only need to do this if the modlet is not (yet) displayed in the mod launcher.
This only applies to *new* modlets - updates to existing modlets should be handled automatically.
New modlets should automatically show up, but there is often a significant delay before they do.

1. Create a new "My Mods" as detailed above, but stop at the "Manage Modlets" step.
   This will create an entirely new game folder (by default, in `C:\7D2D\AlphaXX\My_Mods`).
1. Download the source code from the GitLab site, and install it as if you were installing it
   in the vanilla game.
   But instead of using the vanilla 7D2D game folder, use the "My Mods" game folder from the
   previous step.

Once the Mod Launcher "catches up" and recognizes the new modlet, you should be able to install
updates automatically, as if you installed them from the Mod Launcher in the first place.

## License and usage

[![License: CC0-1.0](https://licensebuttons.net/l/zero/1.0/80x15.png)](http://creativecommons.org/publicdomain/zero/1.0/)

I hereby dedicate all of these modlets to the public domain.

> **This license applies only to my own original work.**
> 
> It does _not_ apply to any text, graphics, code,
> or other works created by other people or organizations,
> including any works that I modify in these modlets.
> This applies to any works by The Fun Pimps, other mod/modlet authors,
> or creators of purchased Unity assets.

If I have incorporated or modified the works of any other authors,
they should be credited in the `README.md` file for the modlet which uses those works.
If this is not the case, please let me know, and I will fix this as soon as possible.

The easiest way to contact me is
[through the 7D2D forums](https://community.7daystodie.com/profile/51265-khzmusik/).
I will also respond to messages sent through this site, but it will not be as quickly.

## Help Wanted

I would greatly appreciate any help with translations.
I have included translations for the texts in most modlets,
but they were generated through an online translation tool,
and probably sound ridiculous to native speakers.

Additionally, any help with testing or balancing is welcome.
I do not play multiplayer games, so reporting any multiplayer bugs is especially welcome.

New feature requests are also welcome,
but there is no guarantee that I am able (or willing) to do them.
