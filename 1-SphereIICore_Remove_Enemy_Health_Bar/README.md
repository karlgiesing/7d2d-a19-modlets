# 1-SphereIICore_Remove_Enemy_Health_Bar

Removes the enemy health bar introduced by the SphereII Core modlet.

## Dependent and Compatible Modlets

This modlet is prefixed with a number, so it is loaded after all dependent and compatible modlets.

This modlet is dependent upon the `0-SphereIICore` modlet.

The version number of this modlet contains the latest version number of the modlet dependency
that has been tested with this modlet.
This modlet *might* not work with older or newer versions of the modlet dependency.
An extra number is appended after a period, in order to version this modlet.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.

However, the SphereII Core modlet is **not** an XPath/XML modlet.
It requires SDX in order to function.

SphereII, the creator of the SphereII Core modlet, created a special block in order to enable
or disable the features provided by his modlet.
That block is specified in the modlet's `blocks.xml` file, and is named "ConfigFeatureBlock".
The top-level properties of that block specify categories of features.

The UI-related features are in a property named "AdvancedUI".
The enemy health bar feature is enabled by a property named "UnlockEnemyHealthBar".
This modlet sets that property's value to `false` using XPath.
