# Human prefabs, quests, and trader lore integration

This modlet ties all the dependent modlets together into a cohesive experience.

## Features
* Separate quest lists for each trader "sympathy" (Duke, Whiteriver, "hostile")
* Duke and Military factions are merged, as per the trader lore

## Dependent Modlets

* `khzmusik_Human_Factions`
    adds the new Vault and Whisperers factions to the game.
    (It also adds the Military faction, but in the Lore modlet,
    military NPCs are part of the Duke faction.)
* `khzmusik_Human_Factions_Prefabs`
    adds the gamestage groups, spawners, and entity groups necessary to create "sleeper" volumes
    for NPCs of different factions.
    It also includes sample prefabs/POIs for RWG.
* `khzmusik_Human_Factions_Prefabs_NPCPackHumans`
    adds humans from Xyth's NPCPack modlet to NPC sleeper volumes.
* `khzmusik_Human_Factions_Prefabs_Quests`
    adds new faction-specific quests.
* `khzmusik_Trader_Lore`
    adds unofficial lore to the trader dialogs.

## Technical Details

This modlet uses XPath to modify XML files.
However, the modlets upon which it depends use custom assets, and/or require DMT.
DMT is *not* compatible with EAC,
and the custom assets must be installed on both servers and clients.

Due to the custom POIs, you should create a new RWG map.
See the `khzmusik_Human_Factions_Prefabs` README for details.

Even if you have a world created with those prefabs,
it is highly recommended to start a new game.
