# khzmusik_No_Bedrolls

On death, this modlet will reset (not remove) skill points, and remove all books read.
It implements this by triggering the same effect used by Grandpa's Forgetting Elixir.

It is designed to be used with "dead is mostly dead" play styles.
In this play style, when players die, they restart as "new characters."
Unlike "permadeath" or "dead is dead," they spawn into the same world in which they died.

For the curious, here is the forum post where I talk about this play style:
> [Mostly Dead: A Manifesto](https://community.7daystodie.com/topic/19135-mostly-dead-a-manifesto/)

## If you do not want to reset books read on death

This is simple.

Go to `Config/buffs.xml` and change:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetProgression" reset_books="true" />
```
...to:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetProgression" reset_books="false" />
```
(Note the different value for the `reset_books` attrubute).