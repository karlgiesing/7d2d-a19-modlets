# Slow Build

Slows down building, and to a lesser extent upgrading.

The purpose of this modlet is to discourage base building, and to encourage renovating existing
POIs for both horde bases and dwellings. Hopefully, this will also make the "nomad" style (moving
from POI to POI) more appealing and practical. The requirement to use renovated POIs as horde
bases should also provide more creativity and challenges.

## Features

* Wood frames now require nails to craft.
  To adjust for this, players are now given a mostly-empty box of nails when starting.
  Also, full boxes of nails (100 count) can sometimes be found in loot.
* Upgrading wood frames requires 10 wood (from 4).
* Crafting cobblestone now requires a cement mixer.
* Crafting flagstone blocks requires 10 cobblestone (from 4).
* Crafting rebar frames requires a crucible in the forge, 15 iron, and 4 clay
  (from 5 iron and 1 clay).
* The stack sizes for all frames have been reduced to 50 (from 500).
* Trees now take twice as long to grow, and yield at most one seed.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.
