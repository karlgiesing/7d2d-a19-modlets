using UnityEngine;
using System.Reflection;

/// <summary>
/// All items in the player's inventory are deleted on death.
/// In other words, the "Drop on death" setting is forced to "Delete all."
/// Ideally this should be triggered when the player enters the game.
/// </summary>
/// <example>
/// <code>
/// &lt;triggered_effect trigger="onSelfEnteredGame" action="ForceDeleteAllOnDeath, Mods"/>
/// </code>
/// </example>
public class MinEventActionForceDeleteAllOnDeath : MinEventActionBase
{
    public override void Execute(MinEventParams _params)
    {
        GameStats.Set(EnumGameStats.DropOnDeath, GetDeleteAllValue());
        Log.Out("DropOnDeath set to DeleteAll");
    }

    private static int GetDeleteAllValue()
    {
        // Try to use reflection to get the protected "DropOption.DeleteAll" enum value 
        var entityType = typeof(EntityPlayerLocal);
        
        var enumType = entityType.GetNestedType(
            "DropOption",
            BindingFlags.NonPublic | BindingFlags.Static);
        
        foreach (var field in enumType.GetFields())
        {
            if (field.Name.Equals("DeleteAll"))
                return (int) field.GetRawConstantValue();
        }
        
        // If reflection didn't work, return the current (A19.3) value
        return 4;
    }
}