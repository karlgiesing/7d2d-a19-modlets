# Mostly Dead

A modlet that implements the "mostly dead" character death style for 7D2D.
For the manifesto, see: https://community.7daystodie.com/topic/19135-mostly-dead-a-manifesto/

This modlet deals only with the player death aspects.
The idea is that when a player character dies, players respawn as an entirely new character,
but they respawn into a (mostly) unchanged world.

## Features

When the player character dies:

* The player's inventory is deleted (the "DropOnDeath" setting is forced to "Delete All").
* Skill points are reset (not removed), and books that were read are forgotten.
* Bedrolls and beds are no longer spawn points, so players do not spawn on them.
* The player map is reset, including any saved waypoints.
* Player quests are removed, and a quest is given that automatically rewards the player with the
  items they start with when starting a new game.
* Player vehicles are destroyed.
* The player's active land claim block is rendered inoperable.
* The player's ACL (Access Control List - i.e. friends list) is cleared.

If you are using this modlet, you do **not** need to install the following modlets:

* `khzmusik_No_Bedrolls`
* `khzmusik_Reset_Skills_On_Death`

Those modlets are now for players who prefer XML-only modlets.

## Technical Details

**This modlet requires DMT.**
It is *not* compatible with EAC.

After installation, it is highly recommended to start a new game.

### Installation

I recommend using the Mod Launcher to install this modlet.
If it does not show up in the Mod Launcher's Mod Downloader screen,
you can just put the mod folder into the 7D2D `Mods` folder, as you would any other modlet.
So long as it is a game managed by the Mod Launcher, the program will automatically detect that
it requires DMT, and do all the necessary compilation.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/4068-the-7d2d-mod-launcher-a-mod-launcher-for-7-days-to-die/

The other option is to use the DMT tool directly.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/13037-dmt-modding-tool/

### Customizing

Nearly all the features are added as triggered effects in the `buffResetPlayerOnDeath` buff.
To remove features you don't like, open this modlet's `buffs.xml` file.
Surround the tags representing the unwanted effects with XML comment tags (`<!-- -->`).

#### Remembering books read

If you want players, after respawning, to remember the books they read,
set the `reset_books` attribute to "false" on the "ResetProgression" action:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetProgression" reset_books="false"/>
```

#### Quest reset customization

The existing implementation resets the player's quests, then gives the player one of many
"StartNewCharacter" quests.
These quests are automatically completed, and reward the player with starting items.
The rewards are tiered, and _approximately_ scaled to the player level.

If you do not want this, you can remove the `effect_group` nodes that have the "PlayerLevel"
requirements in them.
If you still want to remove all the player quests, add a `triggered_effect` with the
"ResetPlayerQuests" action, to the first `effect_group` (the one without the "PlayerLevel"
requirement, where all the other effects are triggered).

These are the custom attributes used when resetting quests:

* `clear_difficulty_tiers` clears the quest difficulty tiers given by the trader,
  making the player start over at Tier I. Defaults to "true".
* `quests_on_reset` is a comma-separated list of quests to give the player when they respawn.
  Defaults to "quest_PlayerRespawn", the quest added by Khaine for his ResetPlayerDMT modlet.

So if you want the "reset quests" feature to behave like the one in the ResetPlayerDMT modlet,
remove the `clear_difficulty_tiers` and `quests_on_reset` attributes from the relevant tag:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, Mods" target="self"/>
```

Or, if you want to send the player on a different quest after death,
set the `quests_on_reset` attribute value to the quest ID.
That attribute can accept a comma-separated list of quest ID, so you can specify more than one:
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, Mods" target="self" quests_on_reset="quest_StartNewCharacter,quest_BasicSurvival1"/>
```

If you don't want the player to start _any_ quests when they respawn,
you can set the `quests_on_reset` attribute value to either an empty string,
or to the special value "none":
```xml
<triggered_effect trigger="onSelfRespawn" action="ResetPlayerQuests, Mods" target="self" clear_difficulty_tiers="false" quests_on_reset="none"/>
```

#### Targeting other players

Many of the new actions accept a target value (in the `target` attribute).
This should almost always be the player ("self"), but it doesn't have to be.
Any of the other target values (like "other" or "selfAOE") will also work,
though only player characters are valid targets.

This could be used for some very weird things.
For example, imagine a club mod called the "ugly stick," used in PvP games.
When your power attack hits another player character with it, they immediately lose their ACL,
meaning they are no longer in the same party with their friends.

This is the effect you would add to that mod:
```xml
<triggered_effect trigger="onSelfSecondaryActionRayHit" action="ClearACL, Mods" target="other"/>
```

Feel free to experiment.

## Thanks to KhaineGB

Most of the code in this modlet was inspired by KhaineGB and his ResetPlayerDMT modlet:
https://community.7daystodie.com/topic/19670-resetplayerdmt-a-hardcore-mode-for-7-days-to-die-a19/

Please stop by that thread to thank him for the hard work and inspiration.

Also, his modlet might be more appropriate for those who want a more "hardcore" experience.
His modlet additionally removes the player's skill points,
and resets the player's experience level.
