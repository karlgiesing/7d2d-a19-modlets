# CreaturePack Humans for Human Factions Prefabs

Adds the CreaturePack Humans to prefabs (POIs) with human faction sleeper volumes.

It is mainly for users who do not want to use DMT modlets.
Unfortunately, without the added DMT features, there are issues - see below.

## Dependent and Compatible Modlets

This modlet is dependent upon these other modlets:
* `0-CreaturePackHumans` (tested with version `19.2.2.3`)
* `khzmusik_Human_Factions_Prefabs` (tested with version `1.0.0`)

It is **not compatible** with `khzmusik_Human_Factions_Prefabs_NPCPackHumans`.
If you want humans that have custom-coded features, install that modlet instead.

While this modlet is compatible with `khzmusik_Human_Factions_Prefabs_Quests_DMT`,
that modlet requires DMT.
If you're using DMT anyway, you might as well use the NPCPack humans.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.

The `0-CreaturePackHumans` and `khzmusik_Human_Factions_Prefabs` modlets,
upon which this modlet depends, include new non-XML resources.
These resources are _not_ pushed from server to client.
For this reason, those modlets should be installed on both servers and clients.

After installation, it is highly recommended to start a new game.

## Known Issues

When spawned in sleeper volumes,
vanilla entity types (whether humans or zombies) will not awaken in the presence of players,
_unless_ players are added to the `SetNearestEntityAsTarget` task.
Unfortunately, this also means they will set the player as an attack target.

I believe it is far better, gameplay-wise, to have humans awaken in the presence of players.
This means _all_ human NPCs will see the player as an enemy.

This is not the case with the NPCPack humans,
since those have custom code that targets entities by _faction._
But, those humans require DMT.

If you do not want all other humans to be enemies, you have two choices:

1. Modify the code in `entityclasses.xml` that makes friendly NPCs view the player as a traget,
    and live with the fact that they will never awaken in human POIs.
    (See the XML file for details.)
2. Use the NPCPack humans and related modlets,
    and live with the fact that you will need to use DMT.
