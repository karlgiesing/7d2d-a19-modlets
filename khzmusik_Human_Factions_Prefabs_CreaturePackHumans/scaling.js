/**
 * @file Generates XML text for entitygroups.xml which adds the NPCPack humans to the custom
 *       entity groups created in the Factions modlet.
 *       It does so programatically, so further edits may still be required.
 */

const factions = buildFactions();
const weapons = buildWeapons();
const gs = buildGameStageStrings();

const tiers = gs.length;

buildNpcs(factions, weapons);

calculateAll(factions);

printResults(factions);

function buildFactions() {
    return {
        bandits: {
            All: [],
            Melee: [],
            Ranged: []
        },
        military: {
            All: [],
            Melee: [],
            Ranged: []
        },
        whisperers: {
            All: [],
            Melee: [],
            Ranged: []
        },
        whiteriver: {
            All: [],
            Melee: [],
            Ranged: []
        }
    }
}

function buildGameStageStrings() {
    return [
        'GS01',
        'GS50',
        'GS100',
        'GS200',
        'GS400',
        'GS800'
    ];
}

function buildNpcs(factions, weapons) {
    factions.bandits.All = weapons.flatMap(weapon => {
        return [
            {
                name: `humanEveBandit${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanCowboyBandit${weapon.name}`,
                health: 200,
                armor: 21.08,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanRaiderHumongous${weapon.name}`,
                health: 500,
                armor: 10.25,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanRaiderMoto${weapon.name}`,
                health: 200,
                armor: 24.92,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanRaiderPest${weapon.name}`,
                health: 200,
                armor: 7.47,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanRaiderRat${weapon.name}`,
                health: 200,
                armor: 14.77,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanRaiderSlugger${weapon.name}`,
                health: 300,
                armor: 7.47,
                weapon: weapon,
                probs: []
            }
        ]
    })
    .filter(filterBandits)
    .sort(npcSorter);
    factions.bandits.Melee = factions.bandits.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.bandits.Ranged = factions.bandits.All.filter(e => e.weapon.isRanged).map(deepClone);

    factions.military.All = weapons.flatMap(weapon => {
        return [
            {
                name: `humanSoldierGirl01Survivor${weapon.name}`,
                health: 200,
                armor: 8.93,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanSoldierDeltaSurvivor${weapon.name}`,
                health: 500,
                armor: 44.65,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanSoldierNationalGuardSurvivor${weapon.name}`,
                health: 500,
                armor: 44.65,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanSoldierRangerSurvivor${weapon.name}`,
                health: 500,
                armor: 44.65,
                weapon: weapon,
                probs: []
            }
        ]
    })
    .filter(filterMilitary)
    .sort(npcSorter);
    factions.military.Melee = factions.military.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.military.Ranged = factions.military.All.filter(e => e.weapon.isRanged).map(deepClone);

    factions.whisperers.All = weapons.flatMap(weapon => {
        return [
            {
                name: `humanWhispererFemale01${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanWhispererFemale02${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanWhispererMale01${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanWhispererMale02${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanWhispererAlpha${weapon.name}`,
                health: 400,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanWhispererBeta${weapon.name}`,
                health: 400,
                armor: 0,
                weapon: weapon,
                probs: []
            },
        ]
    })
    .sort(npcSorter);
    factions.whisperers.Melee = factions.whisperers.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.whisperers.Ranged = factions.whisperers.All.filter(e => e.weapon.isRanged).map(deepClone);

    factions.whiteriver.All = weapons.flatMap(weapon => {
        return [
            {
                name: `humanBakerSurvivor${weapon.name}`,
                health: 200,
                armor: 10.54,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanFarmerSurvivor${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanNurseSurvivor${weapon.name}`,
                health: 200,
                armor: 0,
                weapon: weapon,
                probs: []
            },
            {
                name: `humanSoldierHunterSurvivor${weapon.name}`,
                health: 300,
                armor: 17.86,
                weapon: weapon,
                probs: []
            }
        ]
    })
    .filter(filterWhiteriver)
    .sort(npcSorter);
    factions.whiteriver.Melee = factions.whiteriver.All.filter(e => !e.weapon.isRanged).map(deepClone);
    factions.whiteriver.Ranged = factions.whiteriver.All.filter(e => e.weapon.isRanged).map(deepClone);
}

function buildWeapons() {
    return [
        // {
        //     name: 'EmptyHand',
        //     dmg: 9.1,
        //     isRanged: false
        // },
        {
            name: 'Club',
            dmg: 13.8,
            isRanged: false
        },
        {
            name: 'Knife',
            dmg: 19.8,
            isRanged: false
        },
        {
            name: 'SMG',
            dmg: 64, // doubled for multiple shots
            isRanged: true
        },
        {
            name: 'AK47',
            dmg: 100, // doubled for multiple shots
            isRanged: true
        },
        {
            name: 'Hunter',
            dmg: 50,
            isRanged: true
        },
        {
            name: 'Pistol',
            dmg: 32,
            isRanged: true
        },
        {
            name: 'XBow',
            dmg: 38,
            isRanged: true
        },
        {
            name: 'Bow',
            dmg: 28,
            isRanged: true
        },
        {
            name: 'Shotty',
            dmg: 56,  // assumes per pellet damage
            isRanged: true
        }
    ];
}

function calculateAll(factions) {
    for (var factionName in factions) {
        if (factions.hasOwnProperty(factionName)) {
            var faction = factions[factionName];
            for (var weaponType in faction) {
                if (faction.hasOwnProperty(weaponType)) {
                    const scores = calculateScores(faction[weaponType]);
                    calculateProbabilities(faction[weaponType], scores);
                }
            }
        }
    }
}

function calculateScores(npcs) {
    let maxScore = 0;
    let minScore = Number.MAX_VALUE;
    let total = 0;

    for (var i = 0; i < npcs.length; i++) {
        var weaponScore = npcs[i].weapon.dmg * (npcs[i].weapon.isRanged ? 3 : 1);
        
        npcs[i].score =
            (0.5 * npcs[i].health) +
            npcs[i].armor +
            weaponScore;

        maxScore = Math.max(maxScore, npcs[i].score);
        minScore = Math.min(minScore, npcs[i].score);
        total += npcs[i].score;
    }

    return {
        maxScore,
        minScore,
        total
    };
}

function calculateProbabilities(npcs, scores) {
    const { maxScore, minScore } = scores;
    const range = maxScore - minScore;

    for (let t = 0; t < tiers; t++) {
        // normalized tier: 0..1
        let tN = t / (tiers - 1);
        // tier ratio: -1 to 1, min tier to max tier
        let tR = 2 * tN - 1;

        for (let i = 0; i < npcs.length; i++) {
            // normalized score, 0..1
            let sN = (npcs[i].score - minScore) / range;
            // a line that rotates around (0.5, 0.5), slope determined by tN;
            // the probability is the point on that line at x=sN
            npcs[i].probs[t] = (tR * sN) - (0.5 * tR) + 0.5;
        }
    }
}

function deepClone(obj) {
    return JSON.parse(JSON.stringify(obj));
}

function filterBandits(npc) {
    // Raiders don't use bows
    if (npc.weapon.name == 'Bow' && npc.name.includes('Raider'))
        return false;

    return true;
}

function filterMilitary(npc) {
    // Soldiers don't use bows
    if (npc.weapon.name == 'Bow')
        return false;

    return true;
}

function filterWhiteriver(npc) {
    // Soldier hunter doesn't use bows
    if (npc.weapon.name == 'Bow' && npc.name.includes('Soldier'))
        return false;

    return true;
}

function npcSorter(a, b) {
    return a.name.localeCompare(b.name);
}

function printResults(factions) {
    console.log(`<configs>`);
    for (var faction in factions) {
        if (factions.hasOwnProperty(faction)) {
            let factionName = `${faction[0].toUpperCase()}${faction.slice(1)}`;
            printFactionXml(factions[faction], factionName);
        }
    }
    console.log(`</configs>`);
}

function printFactionXml(faction, factionName) {
    console.log(`\n    <!-- ${factionName} -->`);
    console.log(`    <remove xpath="/entitygroups/entitygroup[starts-with(@name, 'npc${factionName}')]/entity[starts-with(@name,'zombie')]" />`);
    console.log(`    <remove xpath="/entitygroups/entitygroup[starts-with(@name, 'npc${factionName}')]/entity[contains(@name,'animal')]" />`);

    for (var weaponType in faction) {
        if (faction.hasOwnProperty(weaponType)) {
            for (let t = 0; t < tiers; t++) {
                console.log(`    <append xpath="/entitygroups/entitygroup[@name='npc${factionName}${weaponType}Group${gs[t]}']">`);
                faction[weaponType].forEach(npc => {
                    var prob = npc.probs[t].toPrecision(1);
                    if (prob > 0.1)
                        console.log(`        <entity name="${npc.name}" prob="${prob}" />`);
                });
                console.log(`    </append>`);
            }
        }
    }
}
