# Seasonal Weather

A modlet that adds seasonal weather to the game.

## Features

Adds meteorological seasons to the game.
The global temperature will rise and fall according to the seasons.
To make the seasons more distinct, the temperature variation of each biome has been increased.

With the existing values in `worldglobal.xml`, each season is three in-game weeks long
(so, it takes 84 in-game days to get through all four seasons).
The game starts in the middle of the Spring season.
The temperature variation is 31 degrees Fahrenheit, which is taken from hard-coded values in
TFP code.

This is all customizable; see below.

## Technical Details

**This modlet requires DMT.**
It is *not* compatible with EAC.

After installation, it is highly recommended to start a new game.

### Installation

I recommend using the Mod Launcher to install this modlet.
If it does not show up in the Mod Launcher's Mod Downloader screen,
you can just put the mod folder into the 7D2D `Mods` folder, as you would any other modlet.
So long as it is a game managed by the Mod Launcher, the program will automatically detect that
it requires DMT, and do all the necessary compilation.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/4068-the-7d2d-mod-launcher-a-mod-launcher-for-7-days-to-die/

The other option is to use the DMT tool directly.
Details can be found in the 7D2D forums:
https://community.7daystodie.com/topic/13037-dmt-modding-tool/

### Customizing

The minimum and maximum temperatures of all biomes have been adjusted in `biomes.xml`.
If you don't like the new temperature ranges, comment out the relevant XML `set` tags,
or set their values to whatever you like.

These things are all customizable in `worldglobal.xml`:

* Length of the seasonal year (in game days)
* Temperature variation (range)
* Starting season

But there are some caveats.

* The seasonal year does not start until after a "weather grace period."
  The grace period is hard-coded in the game (and is not related to the "newbie coat" buff).
* The global temperature cannot go over 101 degrees, or below 70 degrees (Fahrenheit).
  These limits are hard-coded, and result in a temperature variation of 31 degrees.
  You *can* specify a variation higher than this, but it will mean that you hit the limits sooner
  in each season, and the temperatures swing between these limits faster.
  (If it helps, think of it like clipping in digital audio.)
* When a starting season is specified, players start in the *middle* of that season.
  I chose this because I think it's what most people expect.
  So, if you choose spring (the default), you're starting at the median global temperature,
  and it goes up as you enter into summer.
