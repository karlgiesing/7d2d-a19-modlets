# Quests for NPC Faction Prefabs

New quests that target prefabs (POIs) containing NPCs of a certain faction.
Includes a Reputation mechanic, which is affected by killing NPCs,
and quest rewards that affect the player's reputation.

## New Features

### New Quests
There are several new quests that can be performed on behalf of a certain faction.
Most new quests target prefabs (POIs) containing NPCs of a certain faction.
This is driven entirely by the POI name.

* _Defend:_ defend a human POI against waves of zombies.
    This quest is given by a faction that initially likes or is neutral to the player.
* _Prove yourself:_ prove yourself worthy by killing waves of zombies at a random location.
    This quest is given by a faction that initially dislikes or hates the player,
    since it does not require the player go to the faction's POI (and immediately get attacked).
* _Steal:_ equivalent of a fetch or hidden cache quest.
    The faction giving the quest has been robbed by some other faction (or so they say),
    and you need to steal the stash back.
* _Murder:_ equivalent of a clear quest.
    The faction giving the quest has determined that the humans in another faction's POI
    do not deserve to live.
* ...plus one other, which you should discover for yourself.

### Reputation

If you kill an NPC, your relationship with that NPC's faction decreases. 
But, your relationships with other factions increases or decreases, depending upon the
relationships between those factions and the faction of the NPC you killed.

This includes NPCs that are killed during quests.

Your change in reputation depends upon the specific NPC faction.
For example, non-bandit factions don't care much if you kill bandits,
and even other bandits don't care that much.
On the other hand, killing a Whiteriver NPC will have much larger consequences.

### Reputation books

When read, these books raise your reputation (faction alignment) with one faction.
They are only given as rewards for the new quests, and can't be looted or crafted.
Different tiers of reputation books are given for different quests.

Some books also decrease your reputation with the other factions, depending upon the
relationships between those factions and the faction you just helped.

## Dependent Modlets

This modlet is dependent upon these other modlets.

* `0-CreaturePackHumans`
    is required for `1-NPCPackHumans`.
* `0-SphereIICore`
    adds custom code to specifically target the human POIs,
    and to exclude those POIs from vanilla quests.
    It is also required by `1-NPCPackHumans`.
     _Requires DMT._
* `1-NPCPackHumans`
    is required for `khzmusik_Human_Factions_Prefabs_NPCPackHumans`.
* `khzmusik_Human_Factions_Prefabs`
    adds the factions, gamestage groups, spawners, and entity groups necessary to create "sleeper" volumes
    for NPCs of different factions.
    It also includes sample prefabs/POIs for RWG.
* `khzmusik_Human_Factions_Prefabs_NPCPackHumans`
    adds humans from the `1-NPCPackHumans` modlet to NPC sleeper volumes.
* `SphereII NPC Dialog Windows`
    is required for `1-NPCPackHumans`.

These modlets add further NPC/faction features:

* `2-NPCPackHumans_khzmusik_Adjustments_DMT`
    adds tweaks and adjustments to `1-NPCPackHumans`. _Requires DMT._
* `khzmusik_Trader_Lore`
    adds unofficial faction lore to the trader dialogs.
* `khzmusik_Human_Factions_Quests_Trader_Lore`
    adds these new quests to the traders, consistent with the Trader Lore modlet.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.

However, the dependent modlets _do_ require DMT, and contain custom assets.
They are _not_ compatible with EAC, and must be installed on both servers and clients.
