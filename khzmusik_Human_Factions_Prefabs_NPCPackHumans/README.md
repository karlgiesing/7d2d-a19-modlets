# NPCPack Humans for Human Factions Prefabs

Adds the NPCPack Humans to prefabs (POIs) with human faction sleeper volumes.

The NPCPack Humans have additional faction-related features,
and this modlet adds code to make them behave as expected in sleeper volumes.

However, both the NPCs and this modlet require DMT,
which may be a deal-breaker for some users.

## Dependent and Compatible Modlets

This modlet is dependent upon these other modlets:
* `1-NPCPackHumans` (tested with version `19.2.3.5`)
* `khzmusik_Human_Factions` (tested with version `1.0.0`)
* `khzmusik_Human_Factions_Prefabs` (tested with version `1.0.0`)

This modlet *might* not work with older or newer versions of the modlet dependencies.

It is **not compatible** with `khzmusik_Human_Factions_Prefabs_CreaturePackHumans`.

The `1-NPCPackHumans` modlet has these dependencies, so they will also need to be installed:
* `0-CreaturePackHumans`
* `0-SphereIICore`
  **Note:** You *must* have the latest version!
  It includes a fix to make human sleepers immediately wake up when spawned in sleeper volumes.
  Otherwise friendly NPCs will never awaken.
* `SphereII NPC Dialog Windows`

These modlets are not required, but they add additional human NPC features that you may enjoy:

* `2-NPCPackHumans_khzmusik_Adjustments_DMT`
* `khzmusik_Human_Factions_Prefabs_Quests`
* `khzmusik_Trader_Lore`

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.

The `0-CreaturePackHumans` and `khzmusik_Human_Factions_Prefabs` modlets,
upon which this modlet depends, include new non-XML resources.
These resources are _not_ pushed from server to client.
For this reason, those modlets should be installed on both servers and clients.

After installation, it is highly recommended to start a new game.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.

**This modlet requires DMT.**
It is *not* compatible with EAC.

After installation, it is highly recommended to start a new game.
