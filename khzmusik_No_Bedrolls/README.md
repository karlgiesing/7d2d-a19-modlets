# khzmusik_No_Bedrolls

This is a 7D2D modlet that will remove player-usable bedrolls and beds from the game.
Specifically:

* Bedrolls and beds no longer act as player spawn points, and no longer prevent zombie spawns.
  *Note:* They are still craftable, now they are merely ornamental
* Bedrolls are removed from loot groups.
* The first "basic survival" quest, to craft a bedroll, is removed (the second quest grants extra
  XP to compensate).

It is designed to be used with "dead is mostly dead" play styles.
In this play style, when players die, they restart as "new characters."
Unlike "permadeath" or "dead is dead," they spawn into the same world in which they died.

For the curious, here is the forum post where I talk about this play style:
> [Mostly Dead: A Manifesto](https://community.7daystodie.com/topic/19135-mostly-dead-a-manifesto/)