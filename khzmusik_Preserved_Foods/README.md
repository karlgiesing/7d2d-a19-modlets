# Preserved Foods

Adds craftable canned goods and preserved foods.

## Features

* Added recipes for the vanilla canned foods that are used in advanced recipes
* Added new canned vegetables, with recipes - unlocked at Master Chef tier 4
* Added new preserved berries, with recipes - unlocked at Master Chef tier 3
* Added versions of vanilla recipes that use canned vegetables and preserved berries

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.

However, the modlet also includes new non-XML resources
(new icons).
These resources are _not_ pushed from server to client.
For this reason, the modlet should be installed on both servers and clients.

### Re-use of new assets

The icon images are derivative works of The Fun Pimps original images.
I do *not* claim any rights over these images.

You may only re-use the images under the same terms and conditions that you would
use the original images from The Fun Pimps.

## Possible improvements

* Better localization - I need help with this!
