# 7D2D Development Notes

## Function keys

* `F1`: open command console
* `F2`:
* `F3`: press f3, then use the button called ply on the menu that appears and on the right you can search and see Cvars and active buffs.
* `F4`:
* `F5`: toggle third person
* `F6`: toggle entity spawner
* `F7`: toggle HUD
* `F8`: cycle information (framerate, etc)
* `F9`: take screenshot (these will be in root 7D2D folder)
* `F10`:
* `F11`:
* `F12`:

## Useful Shortcut keys
(debug mode)

* `0` (number pad): toggle entity (zombie/NPC) info
* `*` (number pad): toggle AI
* `p`: toggle camera lock
* `ctrl-right-click`: teleport (in map)
* `alt`: toggle first/third person camera

## Useful Console commands
https://7daystodie.gamepedia.com/Command_Console

* `cm`: toggle creative menu
* `dm`: toggle debug mode
* `exportcurrentconfigs`: export (possibly modified) configuration XML to
  `Saves/<worldname>/<savegamename>/Configs`
* `gfx af <0 or 1>`: disable/enable anisotropic filtering
* `buff <buff name>`: give buff; if name not given, lists available buffs 
* `debuff <buff name>`: remove buff; if name not given, lists active buffs on player

## POI editor

* `<shift>-<alt>-<right click>`: when looking at a trader block, selects the trader
    * Make sure you hit the (nearly invisible) "OK" button in the lower left