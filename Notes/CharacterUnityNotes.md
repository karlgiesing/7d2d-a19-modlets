# Notes about creating characters in Unity

## Make and rig the character
covers Fuse and MakeHuman

### Fuse
Easy to use. Once a character is made:

1. Export as `.obj`
    * Use 2048 for image size, to get only 4 images
    * Make separate folder
2. Upload `.obj` to Mixamo
    * **Only** upload the `.obj` - don't bother zipping up the textures
3. **Before adding any animations,** download `.fbx`
    * Make sure T-Pose is selected
    * Make sure you're exporting for Unity (there are other `.fbx` types, don't use those)
4. Save `.fbx` in the same folder as `.obj` and textures
5. Copy that folder to Unity `Assets` directory
    * Remove `.obj` so you don't get confused!

### MakeHuman
There are two options:

1. Export as `.obj`, rig with Mixamo, as with Fuse
    * Use **centimeters** when exporting from MH -
        for some reason it's at 1/100th scale when exported from Mixamo...?
2. Rig in MH itself and export as `.fbx`
    * Use **meters** as the units if doing this (I think...)
    * Use "game" rig
    * Use T-pose
    * Can be exported directly to Unity `Assets` folder
    * You will need to use the "MakeHuman" controller in XML

## Unity
https://www.youtube.com/watch?v=jLJLZsYNySw&list=PLrSIBJY8f7CEYRUaD8EFf13Koe2VMqgRW&index=3&ab_channel=DavidTaylor

1. Transfer all necessary files to the `Assets` folder in the Unity project
    * I've been putting them in `Assets/Entities/[model name]_KHz`
    * `.fbx` file from Mixamo (or MakeHuman - still testing)
    * image files from MH (if not in Mixamo `.fbx`) or Fuse

2. Upack the prefab

3. Move hips to `Origin`
    * Right-click -> Create Empty -> rename to `Origin`
    * Make sure it's off of base
    * Drag `mixamorig:Hips` to `Origin`

4. Add empty `Footsteps` object as child of `Origin`

...Xyth copies `Collider` to here, but I'm going to wait until after FBX export and re-import 

5. Get rid of `mixamorig:` prefix
    * Drag into Assets folder somewhere to make prefab
    * Right-click, "Show in explorer"
    * Open prefab in text editor, and replace `mixamorig:` with nothing
    * Upon saving the prefab, the character in the scene should automatically update

6. Export as .fbx to fix avatar
    * Right-click, "Export to FBX"
    * Uncheck "compatible naming" but the rest can be the same

7. Set .fbx properties
    * Rig -> Change animation type to "Humanoid"
    * Click "Configure" (save if prompted)
    * Pose -> Enforce T-pose
    * **Important!** Set mesh to read/write
        * Model -> under Meshes section
        * If you don't do this, the entities won't catch on fire

8. Add back to the scene
    * Make sure you unpack the prefab again
    * Rename it with `_KHz` at this point (if not already)

9. Set controller to `DefaultZombieController02`
    * Important - the one in A18 doesn't work any more!
    * Xyth gave me that from Snufkin (where he got it I don't know)
    * Test that it worked by viewing some animations in the lower tab
    * For humans, it's `XythsA19HumanController` (copy, where `IsHuman` is 2)

10. Set up ragdoll
    * Game Object -> 3D Object -> Ragdoll...
    * Hips -> Pelvis
    * Upper Leg -> Hips
    * Leg -> Knee
    * Foot -> Foot
    * Arm (NOT shoulder) -> Arm 
    * ForeArm -> Elbow
    * Spine1 -> Middle Spine
    * Head -> Head
    * Use same Mass as equivalent vanilla zombie (remember this!):
        * male/template: 170
        * female/nurse: 130
        * lumberjack: 230
        * fat female: 260
        * fat hawaiian: 250

11. Adjust all the box colliders on the ragdoll objects, above
    * Copy the `Collider` object, with the `LargeEntityBlocker` tags and the capsule collider
      (what Xyth did prior to exporting as FBX) - doing it now means not re-adding the collider

12. Tags
    * Tag the entire entity as E_Enemy for "killall" if it's a zombie
    * Origin -> E_BP_BipedRoot
    * Collider -> LargeEntityBlocker/LargeEntityBlocker
        _(Only if you didn't copy it after importing  the `.fbx`)_
    * Hips -> E_BP_Body
    * LeftUpLeg -> E_BP_LLeg
    * LeftLeg -> E_BP_LLowerLeg
    * RightUpLeg -> E_BP_RLeg
    * RightLeg -> E_BP_RLowerLeg
    * Spine1 -> E_BP_Body
    * LeftArm -> E_BP_LArm
    * LeftForeArm -> E_BP_LLowerArm
    * Head -> E_BP_Head
    * RightArm -> E_BP_RArm
    * RightForeArm -> E_BP_RLowerArm

13. Gore
    * Copy from nurse...
    * If you don't, you'll have to assign them the gore tags

14. Move gore objects to correct position on model - use Move tool

14. Choose a part with a skinned mesh renderer that covers the largest area on the character,
    and rename it `LOD0`
    - Apparently the game needs a mesh named "LOD0" in order to render flame particles
    - Note that particles will _not_ be rendered on any other mesh

15. For parts with a skinned mesh renderer, extend it out about a body's length

16. Add textures
    * Drag the diffuse map to the part of the model it's texturing
    * For packed images, drag the same diffuse map to all parts of the model
    * In one part (any part) go to the shader, and select the normal map
    * Do not use specular - doesn't work!
    * ~Turn off Specular Highlights~ - Not needed with linear lighting
    * Enable GPU Instancing

17. Save prefab
    * I've been keeping them in a "FinishedPrefabs" folder

18. Build `.unity3d` bundle
    * Select all prefabs to package them into the same bundle
    * Right click, "Build Multi-Platform Asset Bundle"

### Humans

#### Notes from stream with Darkstardragon

```
so, in general, blue down at 90% from hand, red up at 90% from hand, green back at 90% from back side of hand?
(that's "degrees")
90 at Y and Z to start - good rule of thumb
same values for gun joint and melee - ?
"out" meaning away from the hand

"IsHuman" = 2 - got it
"Always animate"  for non-humans, "cull" for humans - got it
```

#### Stream 10/16/2021

* Using Xyth's controller with animations included
* Animals: should have rigs and animations
    * "Animal packs deluxe" packs are used often - not free
* A20 will need a new Unity project - based on 2020.3 I think?
* Bone structure: see above
    * needed for `physicsbody.xml` -
        if absolutely necessary, add a new entry with the bone names
* Bone entities for weapons - A19
    * LeftWeapon: child of LeftHand
    * RightWeapon: child of RightHand
    * GunJoint: child of RightHand (for ranged weapons)
        * It's `Gunjoint` in the XML (note the lower case "j")
    * Blue arrow: pointing down length of arm, outwards
    * Red arrow: "forward" away from wrist
    * Yellow arrow: "up" at 90deg
* A20: Put weapons into model, onto hands
    * Rotate them in Unity
    * Make them invisible by "un-tagging" them
    * Buffs and cvars will "re-tag" them
* Try checking "Cull export transforms" (?)
* Controller is Xyth's Universal controller
    * Parameter "IsHuman" should be 2
        (Why 2? Don't know)
    * Animations are saved with the controller, not the character
        (so if you change them, copy to a new controller)

#### A20

* Unity version: `2020.3.14f12`

## XML

### `entityclasses.xml`

Template:
```xml
<entity_class name="[YOUR ENTITY NAME]" extends="zombieTemplateMale">
    <property name="Tags" value="entity,cp,male,zombie,walker" />
    <property name="Mesh" value="#@modfolder:Resources/[YOUR BUNDLE].unity3d?[YOUR PREFAB NAME]" />
    <property name="Faction" value="undead" />
    <property name="PhysicsBody" value="mixamoConvertedStandardZombie" />
    <property name="RootMotion" value="True" />
    <property name="HandItem" value="meleeHandZombie001" />
    <property name="HasRagdoll" value="true" />
    <property name="Mass" value="170" />
    <property name="WalkType" value="7" />
</entity_class>
```

* The path in the "Mesh" value should be:
    * `#@modfolder:Resources/` to select the modlet's resources folder
    * `[YOUR BUNDLE].unity3d` is the name of the bundle you created, e.g. `Zombies_KHZ.unity3d`
    * `?[YOUR PREFAB NAME]` is the name of the prefab within that bundle, e.g. `ZombieMale_KHz`
* Make sure you use one of the melee hand items from the CreaturePack -
    these fix some odd animation issues due to the "HoldType"
* Make sure "Mass" is set to the same value you set when creating the ragdoll
* A good convention is to name your entity the same as the prefab,
    except in camel-case, with underscores removed:
    `ZombieMaleStripper_KHz` -> `zombieMaleStripperKhz`
    * keep in mind that 7D2D automatically puts spaces before uppercase characters,
        so `zombieMaleStripperKHz` would display as "zombie Male Stripper K Hz" in game