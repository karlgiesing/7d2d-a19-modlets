# Vanilla spawning

## `spawning.xml`

### Entity groups used by biome spawners

* EnemyAnimalsBurntForest - used by burnt_forest
* EnemyAnimalsDesert - used by desert
* EnemyAnimalsForest - used by pine_forest
* EnemyAnimalsSnow - used by snow
* EnemyAnimalsWasteland - used by city_wasteland*, wasteland_hub*, wasteland
* FriendlyAnimalsBurntForest - used by burnt_forest
* FriendlyAnimalsDesert - used by desert
* FriendlyAnimalsForest - used by pine_forest
* FriendlyAnimalsSnow - used by snow
* ZombieDogGroup - used by city* 
* ZombiesAll - used by city*, city_wasteland*, pine_forest, desert, wasteland
* ZombiesBoss - used by city_wasteland*
* ZombiesBurntForest - used by burnt_forest
* ZombiesNight - used by city*, pine_forest, desert, burnt_forest, snow
* ZombiesWasteland - used by wasteland_hub*
* ZombiesWastelandNight - used by city_wasteland*, wasteland_hub*, wasteland

_(* = unused in this alpha)_

### Entity groups used by horde spawners

* SnowZombies - used by SpawnSnowSmall, SpawnSnowMedium, and SpawnSnowLarge
* WolfGroup - used by RoamingHordeSpawnDay
* WolfPack - used by RoamingHordeSpawnDay
* VultureGroup - used by RoamingHordeSpawnDay
* ZombieBikerBarGroup - used by Zombie_Biker_Bar_Med_Territorial
* ZombieDogGroup - used by Dog_Sm_Territorial, Dog_Med_Territorial, RoamingHordeSpawnDay,
* 	and RoamingHordeSpawnNight
* ZombieFarmGroup - used by Farm_Med_Territorial
* ZombieFootballStadiumGroup - used by Zombie_Football_Stadium_Med_Territorial
* ZombieGhostTownGroup - used by Zombie_Ghost_Town_Large_Territorial and
* 	Zombie_Ghost_Town_Sm_Territorial
* ZombieScouts - used by Scouts
* ZombieSoldierGroup - used by Zombie_Soldier_Med_Territorial
* ZombieUtilityWorkerGroup - used by Zombie_Utility_Worker_Med_Territorial and
* 	Zombie_Utility_Worker_Sm_Territorial
* ZombiesAll - used by SpawnExtraSmall, SpawnSmall, SpawnMedium, SpawnLarge,
* 	SpawnExLarge, RoamingHordeSpawnDay, RoamingHordeSpawnNight, and 
* 	HordeMeterDynamicSpawnDay
* ZombiesBurntForest - used by SpawnBurntTown
* ZombiesCave - used by SpawnCave
* ZombiesCrawlers - used by SpawnCrawlers
* ZombiesFeral - used by Spawn_Small_Feral_Territorial
* ZombiesNight - used by RoamingHordeSpawnNight, NightHorde, and 
* 	HordeMeterDynamicSpawnNight
* ZombiesWastelandNight - used by HordeMeterDynamicSpawnDay
* ZombiesWastelandNight2 - used by HordeMeterDynamicSpawnDay
* ZombiesWastelandNightNoBears - used by NightHorde
* ZombiesWastelandNight2NoBears - used by NightHorde

## `gamestages.xml`
_("(N)" = a game stage number)_

### Entity groups used by sleeper groups

* sleeperHordeStageGS(N) - used by S_-Group_Generic_Zombie and S_-_Group_Test_Chamber_Decoy
* scoutHordeStageGS(N) - used by S_-_Group_Abandoned_House
* ZombieBikerBarGroupGS(N) - used by S_-_Group_Biker_Bar
* ZombieFootballStadiumGroupGS(N) - used by S_-_Group_Football_Stadium
* ZombieGhostTownGroupGS(N) - used by S_-_Group_Ghost_Town
* zombieHospitalGroupGS(N) - used by S_-_Group_Hospital
* zombieLabWorkerGroupGS(N) - used by S_-_Group_Lab_Worker
* zombieSoldierGroupGS(N) - used by S_-_Group_Zom_Soldier
* badassHordeStageGS(N) - used by S_-_Group_Zom_Badass
* badassOnlyHordeStageGS(N) - used by S_-_Group_Zom_Badass_Only
* zombieSnowGroupGS(N) - used by S_Zom_Snow
* ZombieHazMatOnlyGroup - used by S_Zom_HazMat_Only
* zombieUtilityWorkerGroupGS(N) - used by S_Zom_Utility_Worker
* zombieBusinessManGroupGS(N) - used by S_Zom_Businessman
* ZombieBurntGroup - used by S_Zom_Burnt
* ZombieBadassGroup - used by S_-_Group_Test_Chamber_Decoy
* ZombieFeralRadiatedGroup - used by S_-_Group_Test_Chamber_Decoy

### Blood Moons

* feralHordeStageGS(N) - used by BloodMoonHorde