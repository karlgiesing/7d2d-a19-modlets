# Scaling NPCs
includes CPHumans and NPCPackHumans

**Note: Vault is done** - no ranged weapons, so scaled in a different way

## Damage by weapon type
Ranked from lowest to highest

### Melee

1. Hand: 9.1 / 0.6
2. Club: 13.8 / 0.4
3. Knife: 19.8 / 0.3
4. Stun baton (vault): 26.2 / 0.2

### Ranged

1. Iron Arrow: 28
2. 9mm: 32 (SMG, pistol)
3. Iron Crossbow Bolt: 38
4. Bandit Rifle: 50
   
   762mm: 50 (AK47)
   
   Shotgun Shell: 11.2 (per pellet? - I assume x5 from roundRayCount)

## Health
(from my own adjustments)

Source property: `HealthMax` - default base_set=200

### Bandits
* `npcEve`: 200
* `npcHeavyGuy`: 300
* `humanRaiderHumongous`: 500
* `humanRaiderMoto`: 200
* `humanRaiderPest`:200
* `humanRaiderRat`: 200
* `humanRaiderSlugger`: 300

### Military
* `npcSoldierGirl`: 200
* `npcSoldierDelta`: 500
* `npcSoldierNationalGuard`: 500
* `npcSoldierRanger`: 500

### Whisperers
* `npcWhisperer[Female/Male]*`: 200
* `npcWhispererAlpha`: 400
* `npcWhispererBeta`: 400

### Whiteriver
* `npcBaker`: 200
* `npcCowboy`: 200
* `npcFarmer`: 200
* `npcNurse`: 200
* `npcSoldierHunter`: 300

## Armor
(from my own adjustments)

Source property: `PhysicalDamageResist` (base_add) - default base_set=10

### Bandits
* `npcEve`: 0
* `npcHeavyGuy`: 14.94
* `humanRaiderHumongous`: 10.25
* `humanRaiderMoto`: 24.92
* `humanRaiderPest`: 7.47
* `humanRaiderRat`: 14.77
* `humanRaiderSlugger`: 7.47

### Military
* `npcSoldierGirl`: 8.93
* `npcSoldierDelta`: 44.65
* `npcSoldierNationalGuard`: 44.65
* `npcSoldierRanger`: 44.65

### Whisperers
* 0 for all of them

### Whiteriver
* `npcBaker`: 10.54
* `npcCowboy`: 21.08
* `npcFarmer`: 0
* `npcNurse`: 0
* `npcSoldierHunter`: 17.86

## Combined Ranks

Formula:

```javascript
// per faction
var npcs = [ /* assume it's built somewhere */ ];

var tiers = 6; // gamestage tiers

var maxScore = 0;
var minScore = 0;
var total = 0;

for (var i = 0; i < npcs.length; i++) {
    npcs[i].score = 
        (0.5 * npcs[i].health) + 
        npcs[i].armor + 
        (npcs[i].weapon.dmg * npcs[i].weapon.isRanged ? 2 : 1);
    
    maxScore = Math.max(maxScore, npcs[i].score);
    minScore = Math.min(minScore, npcs[i].score);
    total += npcs[i].score;
}

for (var t = 0; t < tiers; t++) {
    var tR = 1 - (t / (tiers - 1)); // tiers is a count, not an index

    for (var i = 0; i < npcs.length; i++) {
        var s = npcs[i].score;
        // this was very complicated to work out - used desmos.com/calculater to graph
        npcs[i].prob = (s - (2 * tR * (s - minScore) + (t * range))) / total;
    }
}

```

### Bandits
        <entity name="npcEveClub"/>
        <entity name="npcEveKnife"/>
        <entity name="npcEveSMG" />
        <entity name="npcEveAK47" />
        <entity name="npcEveHunter" />
        <entity name="npcEvePistol" />
        <entity name="npcEveXBow" />
        <entity name="npcEveBow" />

        <entity name="npcHeavyGuyEmptyHand" />
        <entity name="npcHeavyGuyClub" />
        <entity name="npcHeavyGuyKnife" />
        <entity name="npcHeavyGuySMG" />
        <entity name="npcHeavyGuyAK47" />
        <entity name="npcHeavyGuyHunter" />
        <entity name="npcHeavyGuyPistol" />
        <entity name="npcHeavyGuyXBow" />
        <entity name="npcHeavyGuyBow" />

        <entity name="humanRaiderHumongousClub"/>
        <entity name="humanRaiderHumongousKnife"/>
        <entity name="humanRaiderHumongousSMG" />
        <entity name="humanRaiderHumongousAK47" />
        <entity name="humanRaiderHumongousHunter" />
        <entity name="humanRaiderHumongousPistol" />
        <entity name="humanRaiderHumongousXBow" />
        <entity name="humanRaiderHumongousShotty" />

        <entity name="humanRaiderMotoClub"/>
        <entity name="humanRaiderMotoKnife"/>
        <entity name="humanRaiderMotoSMG" />
        <entity name="humanRaiderMotoAK47" />
        <entity name="humanRaiderMotoHunter" />
        <entity name="humanRaiderMotoPistol" />
        <entity name="humanRaiderMotoXBow" />
        <entity name="humanRaiderMotoShotty" />

        <entity name="humanRaiderPestClub"/>
        <entity name="humanRaiderPestKnife"/>
        <entity name="humanRaiderPestSMG" />
        <entity name="humanRaiderPestAK47" />
        <entity name="humanRaiderPestHunter" />
        <entity name="humanRaiderPestPistol" />
        <entity name="humanRaiderPestXBow" />
        <entity name="humanRaiderPestShotty" />

        <entity name="humanRaiderRatClub" />
        <entity name="humanRaiderRatKnife" />
        <entity name="humanRaiderRatSMG" />
        <entity name="humanRaiderRatAK47" />
        <entity name="humanRaiderRatHunter" />
        <entity name="humanRaiderRatPistol" />
        <entity name="humanRaiderRatXBow" />
        <entity name="humanRaiderRatShotty" />

        <entity name="humanRaiderSluggerClub"/>
        <entity name="humanRaiderSluggerKnife"/>
        <entity name="humanRaiderSluggerSMG" />
        <entity name="humanRaiderSluggerAK47" />
        <entity name="humanRaiderSluggerHunter" />
        <entity name="humanRaiderSluggerPistol" />
        <entity name="humanRaiderSluggerXBow" />
        <entity name="humanRaiderSluggerShotty" />

### Military
        <entity name="npcSoldierGirl01Knife"/>
        <entity name="npcSoldierGirl01SMG" />
        <entity name="npcSoldierGirl01AK47" />
        <entity name="npcSoldierGirl01Hunter" />
        <entity name="npcSoldierGirl01Pistol" />
        <entity name="npcSoldierGirl01XBow" />

        <entity name="npcSoldierDeltaKnife"/>
        <entity name="npcSoldierDeltaSMG" />
        <entity name="npcSoldierDeltaAK47" />
        <entity name="npcSoldierDeltaHunter" />
        <entity name="npcSoldierDeltaPistol" />
        <entity name="npcSoldierDeltaXBow" />

        <entity name="npcSoldierNationalGuardKnife"/>
        <entity name="npcSoldierNationalGuardSMG" />
        <entity name="npcSoldierNationalGuardAK47" />
        <entity name="npcSoldierNationalGuardHunter" />
        <entity name="npcSoldierNationalGuardPistol" />
        <entity name="npcSoldierNationalGuardXBow" />

        <entity name="npcSoldierRangerKnife"/>
        <entity name="npcSoldierRangerSMG" />
        <entity name="npcSoldierRangerAK47" />
        <entity name="npcSoldierRangerHunter" />
        <entity name="npcSoldierRangerPistol" />
        <entity name="npcSoldierRangerXBow" />



### Whisperers
* 0 for all of them

### Whiteriver
* `npcBaker`: 10.54
* `npcCowboy`: 21.08
* `npcFarmer`: 0
* `npcNurse`: 0
* `npcSoldierHunter`: 17.86