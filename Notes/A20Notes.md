# A20 Notes
Notes about improving things for A20

## Auto Traders and Workstations

* Garage doors still open when the trader is closed
* Trader POI has one block hole in corner next to sidewalk
* Trader Martin needs a secret stash - things like motors, batteries, vehicle parts, vehicles
* Improve RWG? (How?)
* Opening Trade Routes quests should not go to Martin? (Even possible?)
* Possibly make a general "Specialized Traders" modlet:
    * Restaurant
    * Medical
    * Gun/armor store
    * Tool store
    * Library
    * ...Or make the vanilla traders specialize even more

## Quests/Reputation

* Fix NPC quest frequency in default trader quest list (used by Martin for example)
* Start the player on a lower rep with WR and Duke - too easy as is, no need to do quests for them
* See if you can change the text of the introduction - looked before but didn't know as much then
* Maybe all quests should be done for different factions? (Existing localizations are WR)
* Need faction containers for fetch quests (have one for Duke now, add more)
    ...assuming they don't need to be individually placed in the POI editor?
* Use `QuestActionGiveBuffSDX` instead of the books to set faction relationships;
    the "phase" attribute would be the same phase as "return to trader"

## POIs

* Still one or two with smokestacks - remove!
* Make sure there are fences that NPCs can't jump over
* Add more types of spawn groups
    * "Civilian" (maybe find another name - "noncombatant"? Implies off-duty, sleeping, etc)
* Add translations for POIs

## NPCs

* Hired NPCs will no longer talk with you if relationship to their faction drops
* Eliminate "break block" AITask, at least unless attacking
* If SphereII didn't eliminate the "perpetually opening-and-closing door" bug,
    then just don't open doors at all
* Attack only entities in the line of sight
    * Hopefully eliminates bug where hired NPCs will try to break down your building to reach
        zombies that are outside and unseen
* Separate sleeper NPCs from wandering NPCs?
    * Maybe wandering NPCs are their own faction? ("lost" or something)

## QOL modlet

* Books read icon - maybe just change color
* Dyes stack (10)
* Blue barrels yield water, not gasoline
* Reduce time to scrap brass
* Craft pallets of ammo from stacks (not just materials)
* Vehicles are sellable to traders

## Bad Medicine

* ~~Concussion should cause beer-like visuals~~ It does, but only occasionally - more?

## Preserved Foods

* Possibly merge with food spoilage?
* Lower the perk requirements for canned goods and preserved foods by one
    (so, Grandma for preserved foods, Short Order Cook for cannned goods)
    * DONE - needs to be committed
* Add canned pumpkin, recipes
* Reason things are crafted in lots of 5? (To match Sham?) Needed?
* Add preserved meat:
    * Smoked - can be done with campfire and grill but takes a long time;
        other ingredients = wood, coal?
    * Cured - involves salt (not in game...) and nitrate powder, stored in jar;
        also takes a long time but doesn't require heat (so done in backpack)
    * What perk level? Or, maybe add it in different perk?
* Add blueberry "hand pies" (like Hostess fruit pies) that don't spoil
    to replace all fruit pies in loot

## Crop Growth by Weather

* There should be some way to tell the player which crops grow in which temperatures.
    * Stats screen in seeds maybe?
* If seasonal weather doesn't work, switch to "biome" instead?
    * But if it does, incorporate this modlet with seasonal weather
* Add a test for enclosed, and if so, "normalize" towards middle temp
    * See `EntityStats.GetAmountEnclosure` for vanilla implementation
    * But also take into account "EnclosureDetectionThreshold" in worldsurvival.xml -
        stored in `WeatherParams.EnclosureDetectionThreshold`
    * Make sure to explain it in the tips if implemented!
* Animals and winter
    * `HarvestCount` should decrease
    * find some way to spawn fewer "prey," more "predators"
    * no regular bears (they're hibernating), zombie bears only

## Uncategorized

* Increase the number of days for vending machine restock
    * Maybe have this related to the loot respawn rate?
* Nerf gas to maybe 1/10th of what it is now - "Gas Crisis"?
* Rework skill trees?
    * Master Chef under Fortitude
    * Swap The Brawler and Pummel Pete?
    * Make the Int skill point costs consistent with other trees
