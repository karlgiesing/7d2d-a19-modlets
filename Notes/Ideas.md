# Ideas

List of ideas I have about 7D2D mods/modlets

## Auto Repair Shops - DONE

* Create new workbench to craft vehicles/parts
    * See WIP/SuperWorkbench modlet
    * Model is the auto lift one used in the garage POIs
    * Also include "busted" version?
* Add workbench to POIs?
    * Add to `cntWorkstationsRandomLootHelper`?
* Create Auto Repair Shop trader
    * Trader should be possible with UMA since it doesn't need to move...?
        * Add `entity_class`
        * Add to `block name="spawnTrader"`
            ...maybe? seems to be a helper for spawning the NPC itself
        * Add entry to `Localization.txt`
    * Modify garage POI to be trader
    * Add `trader_item_group` that includes:
        * Vehicle resources - `vehiclePartsT0`, `vehiclePartsT1`, `vehiclePartsMixed`
        * Repair tools - wrench, ratchet, impact driver
        * Schematics - "Common" and "Rare"
            * `schematicsVehiclesCommon`

## Slow Build - DONE
Slows down building, but not so much repairing - idea is to fix up POIs, possibly as a "nomad" style

### Crafting building materials
* Wood frame crafting requires nails
* Rebar frame crafting requires crucible
* Cobblestone crafting requires cement mixer
* Iron frame crafting...?
    * Gated behind int?
    * Already requires forge - double the materials reqs?
* Nails cannot be crafted...?
    * Or: gate behind int?
    * TEST: require anvil in forge

### Upgrading
* All upgrade costs doubled (or more?)
    * Iron frames -> reinforced iron requires forged iron?
* Remove some upgrade paths?
    * cobblestone to concrete?
    * reinforced wood to cobblestone?

### Stack sizes
* Significantly reduce all frame stacks (to 10? too little?)

### Other
* Create "box of nails" - in special builder loot
    * see any of the "ammoBundle" items for implementation
    * Replace rebar frames in loot?
    * IRL, a 1-lb box holds ~50 nails
* Possible to pick up workstations without land claim block?
    * Limit to your own workstations, not in POIs - possible?
    * If not - possible to pick up land claim itself?
    
* Nails on entering game
    * **Discovered:** You can set "=N" in "ItemsOnEnterGame..." property to create stacks!

## Seasonal Weather - DONE

This is going to be a tough one.

**Settings**

* Days per seasonal year (is there a term for "seasonal year?")
* Temperature variation (in Nevada it's about 40F, or +/-20F)
* Grace period? - days before seasonal weather kicks in
* Starting season? (with default being "Fall"?)
  - That's very Western though - maybe "high", "low", "increasing", "decreasing"?
  - Trivia: in AZ the hottest day is around July 5, the coldest is around December 28
* Or, maybe just an "offset" can replace grace period + starting season?

**C# code**
```csharp
public void OnGameUpdate() // TODO Could this be in Update() ?
{
    if (WorldEnvironment.Properties == null)
    {
        return;
    }

    var daysPerSeasonalYear = WorldEnvironment.Properties.GetInt("DaysPerSeasonalYear");
    var tempVariation = WorldEnvironment.Properties.GetInt("TemperatureVariation");
    if (daysPerSeasonalYear = 0 || tempVariation == 0)
    {
        return; // Or we could use a default temp variation if days... is set?
    }

    var dayCount = (int) SkyManager.dayCount;
    // If we want to do this on more than a daily basis, get the time of day
    var timeOfDay = SkyManager.GetTimeOfDayAsMinutes(); // returns float
    var totalMinutes = (dayCount * 1440UL) + timeOfDay;

    // If we need to keep track of when we last updated, then check a class var
    if (this.previousMinutes = totalMinutes)
        return;

    // ...use Math.sin() somehow to calculate
    // This code takes into account the amount we previously added to the global temperature
    var currentOffset = CalculateTemperatureOffset(totalMinutes, daysPerSeasonalYear, tempVariation);
    var previousOffset = CalculateTemperatureOffset(this.previousMinutes, daysPerSeasonalYear, tempVariation);
    var deltaOffset = currentOffset - previousOffset;

    WeatherManager.SetGlobalTemperature(WeatherManager.globalTemperature + deltaOffset);
}
```

You need to inherit from `MonoBehavior`:
https://docs.unity3d.com/ScriptReference/MonoBehaviour.html
https://docs.unity3d.com/Manual/ExecutionOrder.html

Registering `OnGameUpdate`:
```c#
// This is a static object so it will most likely be created early
// and should be available in Awake() (inherited from MonoBehavior)
ModEvents.GameUpdate.RegisterHandler(new Action(this.OnGameUpdate));
// Note: `OnUpdate` will be bound to `this` - see `WinFormConnection` ctor
//   (unlike JavaScript)
```

Math calculations:
```c#
public static float CalculateTemperatureOffset(ulong totalMinutes, int daysPerSeasonalYear, int tempVariation)
{
    // Calculate the radians for Math.Sin(r) - use the total minutes as the "arc length"
    // and the minutes per seasonal year as the "circumference"
    var circumference = daysPerSeasonalYear * 1440;
    var radians = (2 * Math.PI * totalMinutes) / circumference;

    var ratio = Math.Sin(radians);

    return (float)(ratio * tempVariation);
}
```

**XML code**
`worldglobal.xml`:
```xml
<config>
    <append xpath="worldglobal/environment">
        <!-- Each season is 4 weeks game time -->
        <property name="DaysPerSeasonalYear" value="112"/>
        <!-- +/-20 degrees Fahrenheit, typical for Arizona -->
        <property name="TemperatureVariation" value="40"/>
    </append>
</config>
```

**Existing code notes**

```C#

// World time (ulong) to day count
float dayCount = World.worldTime / 24000f + 1f;

// World time (ulong) to time of day
float timeOfDay = World.worldTime % 24000UL / 1000f;

// Time of day (float) in minutes
float timeOfDayInMinutes = timeOfDay / 24f * (float)GamePrefs.GetInt(EnumGamePrefs.DayNightLength);

```

**Khaine's suggesting for saving: a cvar on the player** 
```C#
[HarmonyPatch(typeof(EntityPlayerLocal), "Update")]
public class Khaine_EntityPlayerLoacalUpdate_Patch
{                
    public static void Postfix(EntityPlayerLocal __instance)
    {
        ulong worldTime = GameManager.Instance.World.worldTime % 24000UL;
        
        float worldHour = (int)(worldTime / 1000UL) % 24;
        float worldMinute = (int)(worldTime * 0.06) % 60;
        
        float worldMinute2 = (worldMinute / 100);
        
        float gameWorldTime = (worldHour + worldMinute2);
        
        __instance.SetCVar("_worldTime", gameWorldTime);
    }
}
```
(Obviously I would not use Harmony but would get the primary player instance)

### Second implementation

The first one doesn't work as planned because:
* There's no way to save the state of the custom class
* TFP clamps the global temperature to 70 and 101

Alternate implementation:
* Use TFP value `WeatherManager.temperatureGoingUp`
* Algorithm: if it's been changed recently, change it according to the slope of the curve
  (since it's a sine, the slope is its derivative, cosine)
* To make sure TFP don't change it back soon, set `WeatherManager.lastTimeTemperatureDirChanged`
  to the world time - see `WeatherManager.GenerateWeather`

## Trader Lore - DONE

* Whiteriver - base on [real life](https://en.wikipedia.org/wiki/Whiteriver,_Arizona)
    * Before the war, largest settlement in the Fort Apache Indian Reservation
    * Remote enough to be relatively untouched by warfare and nuclear fallout
    * Became a haven for refugees
    * Lots of settlements adopt Whiteriver name
* Noah White/Wright - base on [Noah Collins](https://www.ou.edu/cas/news/news-items/CarlAlbert2019)
  ...or possibly [Noah James Wright?](https://ancestors.familysearch.org/en/LT6M-BFP/noah-james-wright-1928-1996)
    * History:
        * Member of Sigma Nu Alpha Gamma
        * M.S. in Microbiology, supporting Genetics, from the University of Oklahoma
        * Ignorant response: "some kind of healer I guess"
    * Went to Whiteriver as a doctor
    * Took over leadership after the White Mountain Apache Tribal Council was eaten
* The Duke
    * History:
        * Drill instructor (Gunnery Sergeant) in the Marines
        * Portuguese descent - Fernando Braganza ??? (Fernando = conqueror or leader, "duke"; "Braganza" = Portuguese nobility)
        * Recruits named him after John Wayne, thinking it was an insult - he took it as a source of pride
        * Highest ranking military officer alive after the fall of [Camp Navajo](https://militarybases.com/arizona/camp-navajo/)
        * After the fall, rebuilt his base of operations from the ruins of [Twin Arrows Casino](https://www.twinarrows.com/)
    * Wants to re-establish and preserve civilization... according to his rules of order
* Cassadores
    * Recruits and former military who now follow the Duke
    * Named after the [Caçadores](https://en.wikipedia.org/wiki/Ca%C3%A7adores) ... somehow???
        * ...or "Cazadores" - Spanish for "hunters" (Portuguese too, see above)
        * but definitely **not** named after [Twila Cassadore](https://en.wikipedia.org/wiki/Twila_Cassadore),
          nor [Phillip Cassadore](https://www.latimes.com/archives/la-xpm-1985-08-31-fi-24323-story.html)
    * Nobody knows why they're named that way and each person has a different story
    
### Who Knows What:
(from `traders.xml`)

* Trader Joel, carries better armor
    * Knowledge: Cassadores
* Trader Bob, carries more tools and vehicle parts
    * Knowledge: Whiteriver
* Trader Jen, carries more medicine
    * Knowledge: Noah
* Trader Hugh, carries more guns and ammo
    * Knowledge: Duke
* Trader Rekt, carries more food, seeds farming stuff
    * Knowledge: nothing (total ignorance)

### Dialogs

General structure:
- response (subject)
    * default/general statement about subject
        - history?
            * unaware
            * ignorant
            * informed
        - why name? (for Duke, Cassadores)
            * unaware
            * ignorant
            * informed

Notation:
```
* = statement (what the NPC says)
    If the statement is not in quotes it is the value of the ID attribute;
    otherwise it is the literal text
- = response_entry (what the player says)
    If the response_entry is not in quotes it is the value of the ID attribute;
    otherwise it is the literal text
[xxx] = statement context
    ignorant: crass and often misleading (but hopefully funny).
    unaware: "I don't know" or similar
    informed: full knowledge, see above.
    If there is no statement context, it is the default statement.
```

background
- navezgane
    * dialog_trader_statement_navezgane
        - background
- whiteriveroutposts
    * "Whiteriver is the largest settlement of survivors in Arizona. Survivor outposts often name themselves Whiteriver, after the original settlement."
        - "What was it before the apocalypse?"
            * [unaware] "That was before my time."
            * [ignorant] "Some hippie commune or something."
            * [informed] "Whiteriver was the largest settlement in the Fort Apache Indian Reservation. Remote enough to be relatively untouched by global destruction, it became a haven for refugees during the war. After the first Blood Moon wiped out most of the living, survivors flocked to Whiteriver hoping for safety."
- theduke
    * "The Duke became the boss of Navezgane after the apocalypse. He controls the economy and what's left of the military."
        - "What was he before the apocalypse?"
            * [unaware] "I don't know."
            * [ignorant] "A soldier and a patriot, at least according to his followers."
            * [informed] "Gunnery Sergeant Fernando Braganza was a drill instructor for the Marine Corps. When Camp Navajo was overrun in the first Blood Moon, he became the highest ranking military officer in Arizona.\nHe and the remaining soldiers regrouped in the ruins of the Twin Arrows Casino, and it has been their base of operations ever since."
        - "What does he want?"
            * "He wants to re-establish order and restore civilization, and is willing to use force to do it."
            * [unaware] "Power, just like all men who say they're doing God's work."
            * [ignorant] "He thinks he can turn this shithole country back into America. I wish him luck with that."
        - "Why is he called The Duke?"
            * [unaware] "I don't know."
            * [ignorant] "He likes to pretend he's royalty."
            * [informed] "When he was still a drill seargant, his recruits sarcastically named him after John Wayne for his macho swagger.\nInstead of being insulted, he took the name as a source of pride, and has been called The Duke ever since."
- cassadores
    * "The Cassadores are soldiers of the Duke, consisting mostly of former military."
        - "Why are they called Cassadores?"
            * [unaware] "\"Cazador\" means \"hunter\" in Spanish. I have no idea why the Duke would call his soldiers that."
            * [ignorant] "I guess the Duke is a fan of Age of Empires III."
            * [informed] "He named them after the Caçadores Especiais, the old special forces units of the Portuguese Army."
        - "I had a run-in with them earlier."
            * "I'm surprised you're still alive."
            * [ignorant] "Well, whatever you did to piss them off, don't do it again."
- noah
    * "Noah is the de facto leader of the Whiteriver settlement. The Duke does not like him very much."
        - "What was he before the apocalypse?"
            * [unaware] "He was doctor from up north."
            * [ignorant] "Some kind of shaman or medicine man."
            * [informed] "A member of Sigma Nu Alpha Gamma, he earned degrees in microbiology and genetics from the University of Oklahoma. He came to Arizona to help prevent the outbreak, but ended up in Whiteriver after the first Blood Moon."
        - "How did he become the leader of Whiteriver?"
            * [unaware] "That was too long ago to remember."
            * [ignorant] "I guess nobody else wanted the job."
            * [informed] "He took over after the entire White Mountain Apache Tribal Council was eaten."
- nevermind

## NPC Related

* If an NPC dies while you are their leader, increase the hire costs for all NPCs.
    * Add new buff
    * Modify NPC hire cost method to take buff into account

### NPCs into Zombies

* NPCs turn into zombies that look like them
    * nurse
    * farmer
    * cowboy
    * heavy guy -> lumberjack?
    * some soldiers
* Problem: will need zombie versions of NPCs
    * Could get around it by defaulting to the half-corpse one
* Requires SphereII Core's `SphereII_RandomDeathSpawn`
    * ...which works with spawn groups, so we'll need custom groups per NPC model...

### NPC Dialog features
See [NPC Dialog Notes](./NPCDialogNotes.md)

* Change text to "I would like you to join me"
* Allow "tell me about yourself" for non-hired NPCs

#### Deep dialog options
* Create "conversations" with NPCs
* SphereII's dialog editor might help with this
* Issue: Can I merge with the existing dialog?

### NPC Faction Ideas
* Implement "Victim's Revenge" from WIP
  - Make sure you create some way to get "forgiveness"
* Reset all faction relationships upon death
* When nearby, hired NPCs give faction buff to leader/player (using `MinEventRelatedFactions`)
  - requires DMT/Harmony
  - or maybe not? see `DialogRequirementHiredSDX` - could this be used/modified?
  - Possibly just give the buff upon hiring the NPC?
* NPC POIs
  - Bandits in wasteland, maybe burnt forest?
  - Survivor POIs
  - "Colonies" of NPCs - include new faction trader NPCs
    - "Raider Noel"
* NPC quests
  - For new quest lists, create new `quest_list` in `quests.xml`
    and set `npc_info quest_list=` to its ID in `npc.xml`
  - Defend against zombie hordes - see quests to kill zombie types
    - BUT need to see if NPC kills count towards zombie kills -
      if not, it could be a problem if NPCs kill all zombies (of any type)
  - "Clear zombie" quests can be converted to "Kill (NPC type)" by sending player to NPC POIs
  - Delivery quests? e.g. give food, ammo - maybe not with current quest types
    - BUT there is an `ObjectiveExchangeItemFrom` class in C# code (unused)
  - "Let's start a war"
* Different traders have different faction-related quests:
    * Duke/Cassadores sympathetic: fetch/clear whiteriver, bandit, whisperers
    * Noah/Whiteriver sympathetic: fetch/clear duke, military, bandit, whisperers
    * Rogue: fetch/clear whiteriver, duke, military; "let's start a war" quests
      ...maybe bandits & whisperers too?
* "Rewards" include change in faction alignment (involuntary)
    * might not be possible to do as a quest reward without additional code
    * however, reward could be book that changes faction alignment - 
      use `MinEventActionModifyRelatedFactionsSDX`

#### Quest scenarios for "let's start a war"

* Player accepts quest
    * Player does not do the quest immediately (or ever)
* Player completes quest successfully
    * Player does not turn in quest immediately (or ever)
* Player does not complete quest successfully
    * Player declines quest
    * Player fails quest
    * Player dies before turning in the quest

To account for all this, here's the proposed "let's start a war" workflow:
* Player accepts quest:
  given book to change faction, and another book to change it back -
  _must make these books non-scrappable, non-sellable_
    * Player does not do the quest immediately (or ever):
      either they now have two books, or they're permanently the other faction - 
      either outcome is acceptable for gameplay
* Player completes quest successfully
    * Player does not turn in quest immediately (or ever):
      player is permanently the other faction - acceptable gameplay
* Player does not complete quest successfully
    * Player declines quest:
      either they now have two books, or they're permanently the other faction - 
      either outcome is acceptable for gameplay
    * Player fails quest:
      they still have the book to get back to their original faction
    * Player dies before turning in the quest:
      will need to add a triggered effect to change the player back:
      * `<triggered_effect trigger="onSelfDied" action="ChangeFactionSDX, Mods" value="original" />`
      * add to `buffStatusCheck01`
      * make sure reading the book to change them back, after death, is OK
        (it should be - it just reads the original faction from a cvar set when changing)

#### Implementation

Modlet names:
* `khzmusik_NPC_Factions`
* `khzmusik_NPC_Factions_Prefabs`
* `khzmusik_NPC_Factions_Prefabs_0-CreaturePackHumans` - maybe not?
* `khzmusik_NPC_Factions_Prefabs_1-NPCPackHumans`
* `khzmusik_NPC_Factions_Prefabs_Quests_DMT`
* `khzmusik_NPC_Factions_Prefabs_Quests_DMT_Trader_Lore`

Consistent with trader lore, and (later) NPC dialogs

0. Modlet that add/modifies factions
    * New/modified factions:
        * new faction - "military": "*"=neutral, "undead"&"bandit"&"whisperer"=hate
        * new faction - "vault": "*"=neutral, "undead"&"bandit"&"whisperer"=hate
        * new faction - "whisperer": "*"=hate, "undead"=love
        * "undead": "whisperer"=love
        * "duke": "whisperer"=hate
        * "whiteriver": "whisperer"=hate
1. Modlet that creates NPC "sleeper" volumes for prefabs/POIs:
   `khzmusik_NPC_Prefabs`
    * POI conventions for quests:
        * name must contain `"[faction name]_faction_"`, e.g. `"xcostum_duke_faction_prison(by_khzmusik)"` - 
        **case sensitive?** (try to make it not, see below)
        * POI difficulty levels should probably be one higher than if they were zombies,
          especially if they include ranged NPCs
        * Needs fetch/clear tags, as per usual
    * Sleeper volumes created in `gamestages.xml`:
        ```xml
        <group name="S_-NPC_Group_Duke" emptyChance="0" decoyChance="0" decoy1hpChance="0">
            <spawner name="NPCDukeHorde" count="5,6"/>
        </group>
        ```
        Groups, per faction:
            * `_Ranged`
            * `_Melee`
            * all (no suffix)
    * Spawners also created in `gamestages.xml`:
        ```xml
        <spawner name="NPCDukeHorde">
            <gamestage stage="1">
                <!-- Might want to mess with num and maxAlive? see SleeperGSList -->
                <spawn group="NPCDukeGroupGS1" num="1" maxAlive="1" duration="1"/>
            </gamestage>
            <gamestage stage="50">
                <spawn group="NPCDukeGroupGS50" num="1" maxAlive="1" duration="1"/>
            </gamestage>
            <gamestage stage="100">
                <spawn group="NPCDukeGroupGS100" num="1" maxAlive="1" duration="1"/>
            </gamestage>
            <gamestage stage="200">
                <spawn group="NPCDukeGroupGS200" num="1" maxAlive="1" duration="1"/>
            </gamestage>
            <gamestage stage="400">
                <spawn group="NPCDukeGroupGS400" num="1" maxAlive="1" duration="1"/>
            </gamestage>
            <gamestage stage="800">
                <spawn group="NPCDukeGroupGS800" num="1" maxAlive="1" duration="1"/>
            </gamestage>
        </spawner>
        ```
    * Spawn group values are the names of entity groups, in `entitygroups.xml`:
        ```xml
        <entitygroup name="NPCDukeGroupGS50">
            <entity name="someCassadoreNpc" prob="0.85"/>
            <entity name="someCassadoreNpcWithABigGun" prob="0.1"/>
        </entitygroup>
        ```
        * What to populate with at first?
    * **Possible improvements**
        * Harmony code to have 7D2D not ignore awake check?
2. Create quests for NPC POIs:
   `khzmusik_NPC_Prefabs_Quests_DMT`
    * This _will_ require C#/DMT.
    * Possibly create a _new implementation of_ SphereII's `ObjectiveGotoPOISDX`:
        * ~~modify it to get a _list_ of POIs, not just the first one, and to eliminate repeats~~
          Not necessary - will accept csv in "PrefabNames"
        * modify it to use _case-insensitive_ search
        * modify it to avoid POIs already in `usedPOILocations`
            1. Get list of all POIs matching the name
            2. Filter by used POIs
            3. If the filter eliminates all, choose from the original list
        * modify it to use prefabs near player position, not all in world
          (if applicable - uses `GetPOIPrefabs`/`poiPrefabs` and I don't know what populates that)
        * modify it to _also_ use quest tags, difficulty level, etc.
          (if applicable)
    * Probably need to use a new class that _excludes_ POIs of a certain name;
      used for "normal" (non-NPC) quests.
        `if(strPOInames.Any(instance.name.Contains)) // ...ignore POI`
    * Create the quests themselves. Quest names:
        * `"tier[1..5]_[faction name]_clear"`
        * `"tier[1..5]_[faction name]_fetch"`
        * `"tier[1..5]_[faction name]_fetch_clear"`
        * `"tier[1..5]_[faction name]_defend"` (go to POI, protect NPCs against waves of zombies)
    * Create quest lists depending upon trader faction sympathy
        * duke/cassadores:
            * fetch/clear: whiteriver, bandit, whisperer?
            * defend: duke, military
            * "let's start a war": military vs. whiteriver
        * noah/whiteriver:
            * fetch/clear: duke, military, bandit, whisperer?
            * defend: whiteriver, military
            * "let's start a war": military vs. duke
        * rogue:
            * fetch/clear: duke, military, whiteriver, vault, whisperer?
            * "let's start a war": whiteriver vs. duke
    * Create the books that will change the player's faction relationships.
      Use `MinEventActionModifyRelatedFactionsSDX` with a scale of 0.5 (? - test).
    * ~~Disguises - given at the start of "let's start a war" quests~~
      **Replaced by faction schematics** - see above.
      Uses UMA Overcoat slot:
      `<property name="EquipSlot" value="Chest"/>`
      `<property class="UMA">/<property name="Layer" value="3"/>`
      XPath: `//item[child::property[@name="EquipSlot" and @value="Chest"] and child::property/property[@name="Layer" and @value="3"]]/@name`
        * Bandit disguise: armorScrapChest
        * Duke outfit: apparelCoatJacketGeneric (or maybe gold apparelCoatPufferCoat?)
        * Military uniform: armorMilitaryVest
        * Vault jumpsuit: apparelHazmatJacket (?)
        * Human skin suit (whisperers): apparelGhillieSuitJacket
        * Whiteriver jersey (basketball - Alchesay Falcons?): apparelCoatJacketLetterZU
3. Add entities to NPC sleeper volumes
    * Modlet 1 - add CPack NPCs:
     `khzmusik_NPC_Prefabs_0-CreaturePackHumans`
        * Humans might spawn in sleeping - let users know!
    * Modlet 2: add NPCPack NPCs:
     `khzmusik_NPC_Prefabs_1-NPCPackHumans`
        * Important! set "sleeperInstantAwake"
4. Put it all together, consistent with existing Trader Lore modlet:
   `khzmusik_NPC_Prefabs_Quests_Trader_Lore`
    * Change alignment of military NPCs to "duke"
    * Remove "military vs duke" let's start a war quest
    * Set trader quest lists
    * Add military NPCs to "duke" POIs

## QOL Improvements
(As ideas come in, they should be grouped into related ideas and released as a separate modlet)

* Have dyes stack (stack of 10? seems reasonable)
* The return of raw metals
    * Raw materials must be smelted to usable items
    * Can't scrap metal things in backpacks
    * Raw iron (use image from previous alphas?)
    * Raw lead
* ~~Remove all mention of time & date from the game~~
  ~~(improve on Dough's modlet?)~~
  * Dough's modlet is good.
* **Separate modlet:** Custom configuration on options screen (if possible)
  Perhaps something in `XUi_Menu`?
    * Looks like KhaineGB did something like this in Romero mod (and probably elsewhere)
* ~~**Separate modlet:** Random notes that you can find~~
    * Dough's "You've got mail" is good enough.
    * Will need to be able to pick up stuff with "E" key, like you used to do with plants
        * see block `rock01`
    * Probably make this a separate modlet if I do it
* **Separate modlet:** Binoculars
    * Basically the same as a sniper rifle w/scope
    * Needs a model though, so TODO far in future after I learn Unity
* Cooking Workstations - add to Preserved Foods? Renamed as "cooking" or something?
    * Working electric stove? - requires Core for powered workstations
        * cntStoveOld
        * cntWallOven
    * Working microwave (also requires Core for power)
    * Wood stove - cntWoodBurningStove
    * Charcoal grill - cntCharcoalGrillClosed
    * Gas grill? - cntGasGrillClosed
        * ~~If so, could add gas container for fuel~~ No dice - can't specify fuel type for workstations
    * **UBBI has this stuff... do I need another mod?**

* ~~Make it easier to distinguish unread vs. read books~~ **Done** - separate modlet
    * Brought up in this thread:
      https://community.7daystodie.com/topic/24306-mod-that-lets-you-know-youve-already-read-a-book/
    * `<set xpath="/items/item[@name='schematicMaster']/property[@name='ItemTypeIcon']/@value>add</set>`
    * `<set xpath="/items/item[@name='schematicMaster']/property[@name='AltItemTypeIcon']/@value>check</set>`
    * Maybe also try `<property name="AltItemTypeIconColor" value="#00FF00" />` ?

### Bad Medicine - DONE
* Healing yourself with the sewing kit should produce "pain" sounds
    * `<set xpath="//item[@name='resourceSewingKit']/property[@class='Action0']/property[@class='Sound_start']/@value">player1painlg</set>`
    * **But,** needs to be set by player gender - maybe "Player_*painlg1" works?
      See XML.txt - says it works on buffs
* Non-sterile bandages
    * Cloth fragments can only be made into non-sterile bandages in backpack/workbench
    * Sterile (regular) bandages require either:
        * Grain alcohol - in backpack
        * Boiled water - in campfire
    * Non-sterile bandages have a chance of infection with each use
* "Fire Bad!" - buff that does the opposite of `buffExtinguishFire`
  See `buffIsOnFire` - idea is to add to `$buffBurning[item]Duration` - see e.g. `buffBurningFlamingArrow`
    * Gasoline
    * Alcohol
    * Moonshine
    * How to do this with Gin? Modlets shouldn't be dependent
    * `<triggered_effect trigger="onSelfPrimaryActionEnd" action="PlaySound" sound="molotov_explosion" play_in_head="true" />`


### Repair and Upgrade
How much of this is really needed? Do other modlets do the same things?

* Craftable POI doors
    * `commercialDoor[1,2,4]_v[1..3]`
    * `houseFrontDoor[1,2,4]_v[1..3]`
* Locked doors downgrade to broken unlocked doors
  (already done by other modders, but not to _broken_ unlocked doors)
    * Downgrade with `<property name="DowngradeBlock" value="(new broken door block)"/>`
    * Need a new broken unlocked door: see `<property name="Mesh-Damage-1" value="Door/Door_DMG0"/>`
    * Maybe not necessary if we have craftable POI doors...?
* Open cabinets can be "repaired" to closed cabinets
* Realistic block damage - maybe separate modlet?
  Examples:
    * rconcreteBlock -> concreteDestroyed03
    * woodBlock -> burntWoodBlock3
    * Will it work if we downgrade to a helper block?

## Snowberry Love - DONE

* Snowberry seed recipe and snowberry crops - should be in vanilla, only recipe is missing?
* Snowberry tea - has same effect as goldenrod tea
    * "Freshens your breath and calms your stomach."
* Replace blueberries in herbal antibiotics recipes
* Snowberry medical bandage recipe
    * Snowberry cream recipe, like aloe? Or just snowberries?
    * Should require more snowberries than aloe in vanilla recipe
* New item: Grandma's Gin
    * `drinkJarGrandmasGin`: alcohol, snowberries, plant fibers, wood
    * "How intelligent people get drunk and belligerent."
    * unlocked at same level as Grandpa's recipes
    * buff with same effect as `buffBeer`, for 2x duration
    * buff with same effect as `apparelNerdGlasses` item
