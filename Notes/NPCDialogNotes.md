# Notes
(Don't check this in to the repo)

## Default phrases (existing)

"JoinMe","I would like you to join me." - was: "Hire"
"FollowMe","Follow me."
"TellMe","Tell me about yourself."
"ShowMe","Show Me your inventory."
"StayHere","Stay here."
"GuardHere","Guard here."
"Wander","Wander around here."
"SetPatrol","Follow me for your patrol route."
"SetCode","Here is your Patrol Code."
"Loot","Go find loot for me."
"Dismiss","You are dismissed from my service."
"Patrol","Patrol your route."
"done","Never mind."
"LetsTalk","Let's talk."

### Refactored: start responses

"LetsTalk","Let's talk."
    - statement: conversation
"JoinMe","I would like you to join me."
    - requirement: not hired
    - action: open "hire" dialog
"ShowMe","Show Me your inventory."
    - requirement: hired
    - action: execute "OpenInventory" command
"FollowMe","Follow me."
    - requirement: hired
    - action: execute "FollowMe" command
"StayHere","Stay here." (Change to "Wait here"?)
    - requirement: hired
    - action: execute "StayHere" command
"Dismiss","You are dismissed from my service."
    - requirement: hired
    - statement: dismiss
"done",dialog_trader_response_nevermind

#### Refactored: dismiss statement
"Are you sure?"

"Yes"
    - action: execute "Dismiss" command
"No"
    - statment: start

### Refactored: task responses
"What would you like me to do?" (normal)
"Yes, sir." (soldier)
"Jawoll, mein Führer!" (bandit)

"GuardHere","Guard here."
"Wander","Wander around here."
"SetPatrol","Follow me for your patrol route."
"Patrol","Patrol your route."
"SetCode","Here is your Patrol Code." (Does this work?)
"Loot","Go find some loot." (Does this work?)

### Refactored: conversation responses
"What would you like to know?"

* "TellMe","Tell me about yourself."
    * action: execute "OpenInventory" command
* (before) "What did you do before the apocalypse?"
* (group) "What happened to your group?"
* (family) "Do you have any family"?
    * (parents) "What happened to your parents?"
    * (siblings) "Did you have any siblings?"
    * (spouse) "Did you have a spouse?"
    * (children) "Did you have any children?"
* (survive) "How did you survive on your own?" - maybe, only after all others have been asked?

## Common Phrases

"Shit happens."
"Fuck off."
"Fuck you."
"Well fuck you too."
"None of your business."
"Thank you."
"You're welcome."
"That's terrible!"
"I'm sorry to hear that."
"I'm sorry."
"I meant no offence."
"I agree."
"..."
"They didn't make it."
"He died."
"She died."
"I don't want to talk about it."
"Let's talk about something else."
"Yes."
"No."
"OK."
"I have a task for you."
"What would you like me to do?"
"Why?"
"Why not?"
"What happened?"

## Quotes

### Bible
_(All translations English Standard Version unless otherwise noted)_

"Your dead shall live; their bodies shall rise. You who dwell in the dust, awake and sing for joy! For your dew is a dew of light, and the earth will give birth to the dead." - Isaiah 26:19

"And this shall be the plague with which the LORD will strike all the peoples that wage war against Jerusalem: their flesh will rot while they are still standing on their feet, their eyes will rot in their sockets, and their tongues will rot in their mouths." - Zechariah 14:12

"My bones stick to my skin and to my flesh, and I have escaped by the skin of my teeth." - Job 19:20

"I am the resurrection and the life; he who believes in Me will live even if he dies, and everyone who lives and believes in Me will never die." - John 11:25-26

"For the love of Christ controls us, having concluded this, that one died for all, therefore all died; and He died for all, so that they who live might no longer live for themselves, but for Him who died and rose again on their behalf." - 2 Corinthians 5:14-15

"For since by man came death, by man came also the resurrection of the dead." - 1 Corinthians 15:21 - KJV

"For one who has died has been set free from sin." - Romans 6:7

"Then God blessed Noah and his sons, saying to them, “Be fruitful and increase in number and fill the earth. 2 The fear and dread of you will fall on all the beasts of the earth, and on all the birds in the sky, on every creature that moves along the ground, and on all the fish in the sea; they are given into your hands. 3 Everything that lives and moves about will be food for you. Just as I gave you the green plants, I now give you everything." - Genesis 9:1-3

### Johnny Cash
"How well I have learned that there is no fence to sit on between heaven and hell. There is a deep, wide gulf, a chasm, and in that chasm is no place for any man." 

"There was nothing left of me. I had drifted so far away from God and every stabilizing force in my life that I felt there was no hope…My separation from Him, the deepest and most ravaging of the various kinds of loneliness I'd felt over the years, seemed finally complete. It wasn't. I thought I'd left Him, but He hadn't left me. I felt something very powerful start to happen to me, a sensation of utter peace, clarity, and sobriety. Then my mind started focusing on God."

"Trust gets you killed, love gets you hurt and being real gets you hated." - attributed

"I found out that there weren’t too many limitations, if I did it my way."

"God gives us life and takes us away as He sees fit."

### Other Quotes

#### Murder/War

"I have no interest in understanding sheep, only eating them." - Hannibal Lecter

"Cruelty Is A Gift Humanity Has Given Itself." - Hannibal Lecter

"Killing don’t need no reason. This is ghetto. Reason is for rich people. We have madness." - Marlon James, A Brief History of Seven Killings

"We read of killing one hundred thousand men in a day. We read about it and we rejoiced in it -- if it was the other fellows who were killed. We were fed on flesh and drank blood. Even down to the prattling babe. I need not tell you how many upright, honorable young boys have come into this court charged with murder, some saved and some sent to their death, boys who fought in this war and learned to place a cheap value on human life. You know it and I know it. These boys were brought up in it. The tales of death were in their homes, their playgrounds, their schools; they were in the newspapers that they read; it was a part of the common frenzy -- what was a life? It was nothing. It was the least sacred thing in existence and these boys were trained to this cruelty." - Clarence Darrow

"The joy of killing! the joy of seeing killing done - these are traits of the human race at large." - Mark Twain, Following the Equator: A Journey Around the World

"God made them as stubble to our swords." - Oliver Cromwell

"Evil is a point of view. We are immortal. And what we have before us are the rich feasts that conscience cannot appreciate and mortal men cannot know without regret. God kills, and so shall we; indiscriminately He takes the richest and the poorest, and so shall we; for no creatures under God are as we are, none so like Him as ourselves, dark angels not confined to the stinking limits of hell but wandering His earth and all its kingdoms." - Anne Rice

"Trample the weak. Hurdle the dead." - Attila

"For what is war but your usual custom? Or what is sweeter for a brave man than to seek revenge with his own hand? It is a right of nature to glut the soul with vengeance. Let us then attack the foe eagerly; for they are ever the bolder who make the attack. Despise this union of discordant races! To defend oneself by alliance is proof of cowardice. See, even before our attack they are smitten with terror. They seek the heights, they seize the hills and, repenting too late, clamor for protection against battle in the open fields. (...) Let the wounded exact in return the death of his foe; let the unwounded revel in slaughter of the enemy. No spear shall harm those who are sure to live; and those who are sure to die Fate overtakes even in peace." - Attila

"People sleep peaceably in their beds at night only because rough men stand ready to do violence on their behalf." - George Orwell

"Show me where the filthy human is so that I may feed him his organs." - Eoin Colfer, The Time Paradox

"Violence, naked force, has settled more issues in history than has any other factor." - Starship Troopers

"The West won the world not by the superiority of its ideas or values or religion (...) but rather by its superiority in applying organized violence." - Samuel P. Huntington, The Clash of Civilizations and the Remaking of World Order

"It's more useful to have someone fear you than respect you." - J. Cornell Michel, Jordan's Brains: A Zombie Evolution

"In the end, we all die, and nothing really matters." - Richard Ramirez

"We've all got the power in our hands to kill, but most people are afraid to use it. The ones who aren't afraid, control life itself." - Richard Ramirez

"Shut the fuck up, Carl." - meme

"Wow. What a trip! I thought, ‘To taste death, and yet give life.’ Have you ever tasted blood? It’s warm and sticky and nice." - Susan Atkins

"I love looking at dying people. The last smile on a dying face gives me a great thrill." - Marie Emilie Raymond

"If you would gain a throne and hold it, fear not to make of human skulls thy stepping stones." - Taitu Betul, attributed

"Harden thy heart to all pity, all remorse; then shall thy mind and heart be free, without scruple, to gain high aims." - Taitu Betul, attributed

"A heart that is without tenderness of mercy alone can inhabit a body able to endure and to suffer all." - Taitu Betul, attributed

"To me, this world is nothing but evil, and my own evil just happened to come out cause of the circumstances of what I was doing." - Aileen Wuornos

#### Cooking

"The only real stumbling block is fear of failure. In cooking you’ve got to have a what-the-hell attitude." - Julia Child

"Always start out with a larger pot than what you think you need" - Julia Child

"I approach cooking from a science angle because I need to understand how things work. If I understand the egg, I can scramble it better. It’s a simple as that." - Alton Brown

"No rules. Don't be afraid to do whatever you want. Cooking doesn't have to have rules. I don't like it that way." - Masaharu Morimoto

"Remember, it is never the knife's fault." - Daniel Boulud

"It's okay to play with your food." - Emeril Lagasse

"Working in a restaurant means being part of a family, albeit usually a slightly dysfunctional one. Nothing is accomplished independently." - Joe Bastianich

"I always say that I don't believe I'm a chef. I try to be a storyteller." - Jose Andres

#### Farming

"There are only three things that can kill a farmer: lightning, rolling over in a tractor, and old age." – Bill Bryson

“The ultimate goal of farming is not the growing of crops, but the cultivation and perfection of human beings.” – Masanobu Fukuoka

“Farming is a matter of dirt and dung. It is not the kind of thing we look to to find the meaning of human life. It is too ordinary, too inescapably a part of life to be interesting. We know that it has to be done, but see no reason to pay much attention to it. But it is just because farming is inescapably a part of human life that it may provide a clue to what is most basically human, and so a clue to our place within the cosmos.” – Stephanie Nelson

“My grandfather used to say that once in your life you need a doctor, a lawyer, a policeman and a preacher, but every day, three times a day, you need a farmer.” – Brenda Schoepp

“I enjoyed every bit of it — the responsibility, the successes and dealing with everything that came up. There was a real sense of satisfaction in it.” – Bran McCoy, Refuge Dairy

#### Individualism (or not)

"As for me, I am mean: that means that I need the suffering of others to exist. A flame. A flame in their hearts. When I am all alone, I am extinguished." - Jean-Paul Sartre, No Exit

"It's the reductionist approach to life: if you keep it small, you'll keep it under control. If you don't make any noise, the bogeyman won't find you. But it's all an illusion, because they die too, those people who roll up their spirits into tiny little balls so as to be safe. Safe?! From what? Life is always on the edge of death; narrow streets lead to the same place as wide avenues, and a little candle burns itself out just like a flaming torch does. I choose my own way to burn." - Sophie Scholl

## Personalities

### Misc. Stories

"We were looting a house for food, and were surrounded by a horde from outside. I made it out. They're probably dead."

"My father died before the dead rose. My mother was bitten last year. She got infected, and was about to turn. I had to put her down."

### Chef

* (before)
* (group)
    * ("What happened?")
* (family)
* (survive) 

### Cowboy

based on Johnny Cash?

* (before)
* (group)
    * ("What happened?")
* (family)
* (survive) 

### Farmer

* (before)
* (group)
    * ("What happened?")
* (family)
* (survive) "There are only three things that can kill a farmer: lightning, rolling over in a tractor, and old age."

### Nurse

* (before) "This is not a real nurse's uniform, let's leave it at that."
* (group) Became a nurse for real after apocalypse, worked in refugee camp
    * ("What happened?") Everyone she saved was wiped out during first blood moon
* (family) "I don't want to talk about it."
* (survive) "Most people will make deals with you if you can help them."
    * ("And if not?") "You have to find other ways to deal with them."

### Medic
_(currently only "soldier girl")_

* (before) Joined military during last days of the war, right before bombs dropped/dead rose
* (group) Served in her unit as a paramedic
    * (what then?) Everyone she saved was wiped out during first blood moon
* (family) "Do you have any family"
    * (parents) "They died before the war."
        * ("I'm sorry to hear that.") "I think they were the lucky ones."
    * (siblings) "I had two brothers in the military, but they were deployed to other units."
        * ("What happened?") "They're probably dead now."
            * ("I'm sorry to hear that.") "Shit happens."
    * (spouse) "Nobody could deal with the fact that I was stronger than them."
    * (children) "This is not a world for children."
* (survive) "Most people will make deals with you if you can help them."
    * ("And if not?") "You have to find other ways to deal with them."

### Bandit/Raider
_(currently only "heavy guy")_

* (before):
    * Not hired: "There was nothing before the apocalypse."
    * Hired: "I was a high school teacher."
        * ("What happened?") "When the world is ruled by death, only a fool would not become a bringer of death."
* (group) "To defend oneself by alliance is proof of cowardice."
* (family) "I was fed on flesh and drank blood. Even as a prattling babe. What is a life? It is nothing. It is the least sacred thing in existence. You know it and I know it."
* (survive) "We've all got the power in our hands to kill. The ones who aren't afraid to use it, control life itself."

### Female Bandit/Raider
_(currently only Eve)_

based on Inez from No Exit?

* (before) ???
* (group) ???
* (family) ???
* (survive) "A heart that is without tenderness of mercy alone can inhabit a body able to endure and to suffer all."

### Whisperer

* ("Why do you walk with the dead?")
    * "God has blessed Them, saying to Them: Be fruitful and increase in number and fill the earth. The fear and dread of You will fall on all the beasts of the earth, and on every creature that moves along the ground; they are given into Your hands. Everything that lives and moves about will be food for You."
    * ~"One who has died has been set free from sin."~ - used in "murder" quest

* (before) "This was the judgment: the light did come into the world, and people loved the darkness rather than the light because their works were evil. But whoever does what is true comes to the light."
* (group) "The wind blows where it wishes, and you hear its sound, but you do not know where it comes from or where it goes. So it is with everyone who is reborn of the dead souls."
* (family) "Their love controls us, having concluded this, that They died for all, therefore all died; and They died for all, so that those who live might no longer live for themselves, but for Them who died and rose again on their behalf."
* (survive) "My bones stick to my skin and to my flesh, and I have escaped by the skin of my teeth."

### Whisperer Beta

* (before) "I was a famous country music singer."
    * ("Really?") "Just kidding, that would be ridiculous."

### Vault dwellers
_(N/A - can't converse)_
