# AI in 7D2D

## Vanilla code

* `EAITaskList` - static and dynamic tasks
    * used for **both** `EAIManager.tasks` and `EAIManager.targetTasks`
    * `private List<EAITaskEntry> allTasks` - static
        * accessed from public `Tasks` property
        * task added by `AddTask` which is called only when copying from parent entity
          (in `EAIManager.CopyPropertiesFromEntityClass`)
    * `private List<EAITaskEntry> executingTasks` - dynamic
        * accessed from public `GetExecutingTasks` method
    * `EAITaskEntry`:
        * `priority`
            * The priority is determined by (N) in "AITask-(N)"
            * A lower numeric value is a higher (more important) priority
        * `isExecuting`
        * `executeTime`
        * `action` - reference to `EAIBase`
    * `OnUpdateTasks`:
        * used, ultimately, by `EntityAlive.OnUpdateLive`
        * Begin by clearing all started tasks.
        * For each static task (in `allTasks`):
            * If it's executing, see if it's not `isBestTask`
              _or_ it can't `Continue`.
              If this is the case, _permanently stop it:_
              * stop executing it
              * reset its `action`
              * remove it from executing task list
              * move on to the next task
            * If `isBestTask`, and `CanExecute`, add it to started tasks _and_ executing tasks
        * For all started tasks, call `Start`
        * For all executing tasks, call `Update`
    * `isBestTask`:
        * Returns false if there are any "better" tasks. A better task is one which:
            * is currently executing
            * has a **numerically lower or equal** `priority`
            * ~~AND is not continuous (`IsContinuous`)~~ (Always true - implemented in base class and never overridden)
            * AND is **not** compatible (`areTasksCompatible`) with the given task
              * Determined by a binary AND of their `MutexBits` equalling zero -
                only happens if `MutexBits` have **no** bits in common
        * Otherwise return true. 

### AITask types

* `EAIApproachAndAttackTarget`
    * Find a path to the target, and start attacking it when "close enough."
        * Targets are set by "AITarget" tasks, below
    * `MutexBits`: 3 / 0b0011
    * `data`:
        * Target class
        * Max chase time
    * `CanExecute` - false if:
        * sleeping/waking
        * just attacked ("stunned" on body)
        * no attack target
        * attack target type is not in list of target classes
    * `Continue` - false if:
        * sleeping/waking
        * just attacked ("stunned" on body)
        * it's going home (`isGoingHome`), has a home (`ChaseReturnLocation` != 0),
          and has no attack target
        * no attack target, different attack target,
          or attack target is dead and can't be eaten

* `EAIApproachDistraction`
    * Find a path to a distraction
    * `MutexBits`: 3 / 0b0011
    * `CanExecute` - false if:
        * no pending distraction
        * distraction is found and is not "to eat"
        * distraction is not "to eat" and it finds an attack target
    * `Continue` - false if:
        * ...lots of stuff; same kinds of considerations as `CanExecute`

* `EAIApproachSpot`
    * Find a path to a spot to investigate
    * `MutexBits`: 3 / 0b0011
    * `CanExecute` - false if:
        * has no spot to investigate
        * sleeping
    * `Continue` - false if:
        * same conditions as `CanExecute`
        * found spot

* `EAIBreakBlock`
    * Breaks a block (AFAICT must already be targeted)
    * `MutexBits`: 8 / 0b1000
        - _compatible with approach tasks_
    * `CanExecute` - false if:
        * just attacked ("stunned" on body)
        * jumping and not `IsDestroyArea`
        * block is already broken
        * block is too far away
    * `Continue` - false if:
        * same conditions as `CanExecute`
        * not on ground

* `EAIDestroyArea`
    * Destroy! Destroy!
    * `MutexBits`: 3 / 0b0011
    * `CanExecute` - false if:
        * can't break blocks
        * no attack target
        * can't find a block to destroy
    * `Continue` - false if:
        * just attacked ("stunned" on body)
        * no path to block to destroy
        * found block to destroy
            * _NOTE:_ `Continue` actually _finds_ the block to break -
              once found, it should go to the next task, which is `EAIBreakBlock`

* `EAILeap`
    * Used by spider zombie
    * `MutexBits`: 3 / 0b0011
    * `CanExecute` - false if:
        * no attack target
        * jumping
        * leg missing
        * no path to target
        * target too far
    * `Continue` - false if:
        * just attacked ("stunned" on body)
        * just started jumping

* `EAILook`
    * Look around
        * In vanilla, used right before "Wander"
    * `MutexBits`: 1 / 0b0001
    * `CanExecute` - false if:
        * it's looked around long enough
    * `Continue` - false if:
        * just attacked ("stunned" on body)
        * it's looked around long enough

* `EAIRangedAttackTarget`
    * Ranged attack targeting, used by zombie cop
        * In code, if the itemType is 0, it will call `Use`
    * `MutexBits`: 11 / 0b1011
    * `data`:
        * itemType: int
            * `1`: `EntityAlive.Use` (invokes `this.inventory.holdingItem.Actions[1]`)
            * `0`: `EntityAlive.Attack` (invokes `this.inventory.holdingItem.Actions[0]`)
        * cooldown: float - default 3, vanilla cop sets 4, don't know units
        * duration: float - default 20, vanilla cop sets 5, don't know units
        * minRange: float - default 4, I assume meters?
        * maxRange: float - default 25, I assume meters?
    * `CanExecute` - false if:
        * cooldown in effect
        * `Entity.IsAttackValid` is false
        * can't see target
        * target not in range
    * `Continue` - false if:
        * target not alive
        * attack time less than duration
        * entity is attacked

* `EAIRangedAttackTarget2`
    * Ranged attack targeting, not used in vanilla
        * However CPHuman/NPCPHuman templates use it for ranged
        * Likely put in for upcoming bandits
    * `MutexBits`: 11 / 0b1011
    * `data`:
        * itemType: int
            * `0`: `EntityAlive.Use` (invokes `this.inventory.holdingItem.Actions[1]`)
            * `0`: `EntityAlive.Attack` (invokes `this.inventory.holdingItem.Actions[0]`)
        * attackPeriod: float - max. attack period
            * used to determine `_bAttackReleased` value to pass to `Use`/`Attack`
    * `CanExecute` - false if:
        * `this.inventory.holdingItem.Actions` is null
        * `this.inventory.holdingItem.Actions[0]` is not `ItemActionRanged`
        * needs to reload (starts reloading if so)
        * still in attack timeout
        * no attack target
        * target not in range
            * determined by `DamageFalloffRange` of `this.theEntity.inventory.holdingItemItemValue`
        * can't see target
    * `Continue` - false if:
        * still in attack period
        * entity is attacked

* `EAIRunawayFromEntity`
    * Runs away from an entity or entities (by entity class)
    * `MutexBits`: 1 / 0b0001
    * `data`:
        * Target classes ("class=", comma-separated)
        * `safeDistance` (after ";") - default 38f
        * `minSneakDistance` (after ";") - used only with non-player entities; default 3.5f
        * `fleeDistance` (after ";") - maybe? It's in abstract base class, not in vanilla XML
    * `CanExecute` - false if:
        * no target class in range
        * can't find a position to flee to
    * `Continue` - false if:
        * reached `safeDistance`
        * ran away for too long (timeout ticks in base class)

* `EAIRunawayWhenHurt`
    * Runs away from an entity when that entity hurts it
    * `MutexBits`: 1 / 0b0001
    * `data`: `"runChance=0.5;healthPer=0.3;healthPerMax=0.6"`
        * runChance - chance to run (0..1)
        * healthPer - min health "percent" (0..1) to run
        * healthPerMax - max health "percent" (0..1) to run
        * fleeDistance (after ";") - maybe? It's in abstract base class, not in vanilla XML
    * `CanExecute` - false if:
        * no revenge target (nobody hurt it)
        * not below minumum health to flee
        * can't find a position to flee to
    * `Continue` - false if:
        * reached safe distance (hard-coded to 45)
        * no revenge target (nobody hurt it)
        * ran away for too long (timeout ticks in base class)

* `EAITerritorial`
    * Returns to its territory ("home")
        * currently used by zombies spawned in sleeper volumes
    * `MutexBits`: 1 / 0b0001
    * `CanExecute` - false if:
        * already in its territory
    * `Continue` - false if:
        * has no path "and is not planning one"

* `EAIWander`
    * Wander around
    * `MutexBits`: 1 / 0b0001
    * `CanExecute` - false if:
        * sleeping/waking up
        * not around any players for more than 120 ticks
        * just attacked ("stunned" on body)
        * random time is greater than wander time (stops at random)
        * is looking around (already approaching spot or distraction)
        * is alert and has (found) attack target
    * `Continue` - false if:
        * just attacked ("stunned" on body)
        * wander path blocked
        * been wandering "long enough" (30f, don't know units)
        * has no path "and is not planning one"

### AITarget types

* `EAIBlockIf`
    * Blocks if a condition evaluates as true
        * Used in hostile animal template
    * `MutexBits`: 1 / 0b0001
    * `data`: "condition=" plus...
        * type: "alert", "investigate"
        * op: "e" (equals), "ne" (not equals)
        * value: float (usually 1 or 0)
    * `CanExecute` - false if _none_ of the conditions pass evaluation
    * `Continue` - calls `CanExecute`

* `EAIBlockingTargetTask`
    * Blocks entities from "going home" in `EAIApproachAndAttackTarget`
        * In all vanilla examples, is between `SetAsTargetIfHurt`
          and `SetNearestEntityAsTarget`/`SetNearestCorpseAsTarget`
    * `MutexBits`: 1 / 0b0001
    * `CanExecute` - false if `canExecute` is false (as set in `EAIApproachAndAttackTarget`)
    * `Continue` - calls `CanExecute`

* `EAISetAsTargetIfHurt`
    * Sets an entity as its target when that entity hurts it (class-based) -
      the target that hurts it is called the "revenge target"
    * `MutexBits`: 1 / 0b0001
    * `data`:
        * Target classes ("class=", comma-separated)
    * `CanExecute` - false if:
        * the target is already the attack target
        * the attack target is the same class as the entity itself
        * the revenge target's class is not in the list of target classes
    * `Continue` - false if:
        * it doesn't have (need?) a revenge target
        * the base `EAITarget.Continue` returns false (can't find a target at all)

* `EAISetNearestCorpseAsTarget`
    * Sets the nearest corpse as the target (to eat)
        * Any entity other than the player doesn't work; causes NREs, no eating animation
    * `MutexBits`: 1 / 0b0001
    * `data`:
        * Target classes ("class=", comma-separated)
        * maxDistance2d 
    * `CanExecute` - false if:
        * no target class in range
        * already investigating some position (e.g. horde night)
    * `Continue` - false if:
        * it doesn't have (need?) a revenge target
        * the base `EAITarget.Continue` returns false (can't find a target at all)

* `EAISetNearestEntityAsTarget`
    * Sets the nearest entity as the target (to attack)
        * Vanilla uses it for players - can we use it for other corpses? - YES!
    * `MutexBits`: 1 / 0b0001
    * `data`: ("class=", comma-separated)
        * Target class
        * hear max distance (default  0)
        * see max distance (default 0)
        * From XML comments: "checked left to right, 0 dist uses entity default"
    * `CanExecute` - false if:
        * distracted
        * no targets in range
    * `Continue` - false if:
        * distracted
        * target is dead
        * found attack target
