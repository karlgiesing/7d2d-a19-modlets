# Faction Prefabs

New factions, and tools for adding NPCs to prefabs, per faction.

This modlet adds the gamestage groups, spawners, and entity groups
necessary to create "sleeper" volumes for NPCs of different factions.

It also creates new NPC factions.

It does not actually add any NPCs.
Adding NPCs should be done in a separate modlet,
depending upon whose NPCs you want to add.

By default, all the sleeper volumes will spawn vanilla zombies.
So, without adding any NPCs, they will act like "normal" POIs.

## Features

### New Factions

* military: hates bandits, undead, and whisperers; neutral to everyone else
* vault: hates bandits, undead, and whisperers; neutral to everyone else
* whisperers: loves the undead; hates everyone else

The existing faction relationships to these new factions are adjusted accordingly (if needed).

#### Faction Translations

There are two phrases that are translated per faction.

The first is for the literal phrase "the [faction name] faction".
Here, "[faction name]" is the _adjective_ describing the faction.
This should be used when translating the faction itself, not the humans in the faction.

The key for this phrase is "faction_[faction name]".

The second is for the name of humans in the faction.
These translations are for _noun versions_ of the name:
* Proper nouns are singular: "the Duke", "Whiteriver".
* The military faction is singular collective: "the military".
* Other nouns are plural: "the bandits", "the whisperers", "the vault dwellers".

The key for this phrase is "faction_name_[faction name]".

Note that in English, singular nouns can also be used as adjectives:
"the Duke clan", "Whiteriver supplies", "military weapons".
This is usually _not_ true of other languages.
This is why "the [faction name] faction" is provided as a separate translation.

### Faction Sleeper Groups

These factions all have gamestage groups associated with them:

* bandits
* duke
* military _(new)_
* vault _(new)_
* whisperers _(new)_
* whiteriver

For details on their use, see the [Design README](./DESIGN_README.md).

## Related modlets

This modlet provides the sleeper volumes and associated spawn groups.
It should be installed by _both prefab creators and users._

If this modlet is not installed, _nothing_ will spawn in human POIs,
because the game will not recognize the sleeper volumes.

If _only_ this modlet is installed:

* Zombies will spawn in human POIs.
    You need another modlet to populate the sleeper volumes with human NPCs.
    I provide two modlets that do this:
    * `khzmusik_Human_Factions_Prefabs_NPCPackHumans` uses NPCPack humans
    * `khzmusik_Human_Factions_Prefabs_CreaturePackHumans` uses CreaturePack humans
        (for those players who can't or won't use DMT)

* Human POIs will be targeted by vanilla "clear" or "fetch" quests, depending upon its quest tags.
    You need another modlet to create the NPC-related quests that target your POI,
    and to tell the vanilla game not to target your POI for its own quests.
    My `khzmusik_Human_Factions_Prefabs_Quests` modlet does this.

In other words - if you include only this modlet, human POIs will behave just like vanilla POIs.

This is to make the modlet (and POIs) more versatile.
For example, let's say someone is adding NPCs, but they don't have any "vault" NPCs.
They can simply do nothing to the vault-related entity groups,
and the vault POIs will spawn zombies instead.
Or, they could add bandit NPCs if they thought that was more appropriate.

If other modlet authors create their own modlets that populate the sleeper volumes,
or provide their own NPC-related quests,
then players can install those modlets rather than mine.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.

However, the modlet also includes new NPC POI prefabs.
These prefabs are _not_ pushed from server to client.
If you are using these POIs, the modlet should be installed on both servers and clients.

### KingGen

The `KingGen-poislist.txt` file contains text to _append to an existing_
`poislist.txt` file, used by the KingGen random world generator tool.

It is **not** a complete `poislist.txt` file!
Do not try to load it into KingGen as is.
It contains _only_ the data for the new prefabs, and should be appended after the `@pois` tag.

### NitroGen

The `NitroGen-prefablist.txt` file contains text to _append to an existing_
`prefablist.txt` file, used by the NitroGen random world generator tool.

It is **not** a complete `prefablist.txt` file!
Do not try to load it into NitroGen as is.
It contains _only_ the data for the new prefabs, and should be appended at the end of the file.

### Removing the sample POIs from RWG

You might not want the sample POIs in your world.
(For example, if others have made better POIs, you might want those instead.)

In this case, comment out all the XML lines in `rwgmixer.xml`.
The POIs will not be discovered by 7D2D, and will not be included in a new world.

If you have already created a world with these prefabs in them,
then you must start a new world,
or replace the sample POIs by hand using the world editor.

If your worlds do not include the sample POIs,
then this modlet does *not* need to be installed on both clients and servers.
The rest of the modlet is just XPath,
so the XML changes should be pushed to the clients.

(If you are using another modlet that adds its own POIs,
_that_ modlet needs to be installed on the clients.)

### Adding different NPCs

Other modders are encouraged to add their own NPCs to the entity groups used by NPC POIs.

To add NPCs:

1. Specify that your modlet is dependent upon this modlet
   (e.g. in your README or ModInfo.xml).
2. Remove the existing zombies and animals (vultures) from the NPC entity groups.
   Example:
   ```xml
   <!-- in entitygroups.xml -->
   <remove xpath="/entitygroups/entitygroup[starts-with(@name, 'npc')]/entity[starts-with(@name,'zombie')]" />
   <remove xpath="/entitygroups/entitygroup[starts-with(@name, 'npc')]/entity[contains(@name,'animal')]" />
   ```
3. Add your NPCs to the appropriate entity groups.
   By convention, they are named `npc[Faction][All|Ranged|Melee]GroupGS[01|50|100|200|400|800]`.
   The numbers at the end represent the gamestage.

### Creating POIs/prefabs

I encourage prefab designers to create their own human POIs.

I created an entirely separage README for prefab designers here:

[DESIGN_README](./DESIGN_README.MD)
