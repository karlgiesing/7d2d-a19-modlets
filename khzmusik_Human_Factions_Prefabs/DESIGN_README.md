# For POI Designers

This README is for others who want to design POIs (prefabs) with human NPCs.

## How to use this modlet

This modlet provides groups for these factions:

* bandits
* duke
* military _(not in vanilla)_
* vault _(not in vanilla)_
* whisperers _(not in vanilla)_
* whiteriver

There are also melee and ranged versions of each faction group.

To use the provided NPC sleeper volumes:

1. Select the sleeper volume(s) in your POI, and hit "k" to view their details.
2. The new NPC gamestage groups should show up as options for the sleeper volume.
   They are displayed as `Group NPC [Faction] [All|Ranged|Melee]`.
3. Select the value for `xuiSleeperVolumeTrigger` as appropriate.

*Note:* This modlet does not actually add the factions to the game.
The faction names in the sleeper volumes are just names;
the factions of any entities within them are not enforced in any way.
See the main [README](./README.md) for details.

### Sleeper volume trigger values

This is how the sleeper volume triggers are supposed to affect sleeping entities:

* `Hostile`: when spawned in, the entity will be awake, and looking for a target to attack.
    **Important:** Hostile humans _will_ attack the player, no matter their faction alignment!
* `Active`: when spawned in, the entity will be asleep, but "aware."
    It will awaken if it sees or hears anything (subject to player buffs).
    This is the normal sleeper volume setting for vanilla POIs.
* `Passive`: when spawned in, the entity will be asleep and "unaware."
    It will not wake up unless it is attacked.

I recommend using `Active` in most situations.

Use `Hostile` only for locations where a human would attack _anyone,_
even a member of their own faction.
If you do use `Hostile` sleeper volumes, I recommend that you _do not_ put quest loot in them.

Use `Passive` only for humans that act as "decoration."
For example, I have used `Passive` for humans that are locked in jail cells
unreachable by the player.

**Important note:** This is how entities are _supposed_ to behave in sleeper volumes.
The actual NPCs may or may not act like this, for technical reasons.
Even so, it's better to design the sleeper volumes as if the NPCs behave as they should.

### POI design considerations

Human POIs are conceptually different than vanilla POIs.
This is my _advice_ about designing POIs with humans in them.

* It is probably a good idea to give some visual indication that humans live at the POI.
  Here are some ideas I have used:
    * Whiteriver flagpoles for whiteriver POIs, American flagpoles for military POIs
    * The POI is (relatively) livable: most lights are on, there are few garbage piles, etc.
* Human NPCs have a tendency to wander around once woken.
  If you want the NPCs to stay within the POI, it should have a barrier of some kind.
  Some NPCs can open unlocked doors, so they are not an effective barrier.
* Human NPCs do not avoid traps very well (or at all).
  Do not put traps inside any parts of the POI that NPCs can get to.

You may also want to read the prefab rules for inclusion in Compopack.
They are in this Discord channel (you will have to sign in to enter):
https://discord.com/channels/743253634556493874/755802500241555467

## Naming conventions for quest targeting

The `khzmusik_Human_Factions_Prefabs_Quests` modlet targets POIs **by prefab name.**
(This is unavoidable; it is impossible to add new POI quest tag values to the vanilla game.)

To be targeted by its quests, your prefab name must contain the string "[faction]_faction".
For example, all these POIs will be targeted by quests about Whiteriver NPCS:

* `whiteriver_faction_camp01`
* `xcostum_whiteriver_faction_house(by_khzmusik)`
* `I love the whiteriver_faction`

If you don't follow this naming convention, **your POI will be treated like a vanilla POI.**
It will not be targeted for NPC quests, and it will be targeted for vanilla quests.

Please note that most NPC quests _also require quest tags._
For example, a "murder the bandits" quest is essentially a "clear" quest targeting a bandit POI.
That POI will need the "clear" tag in its XML:
```xml
<property name="QuestTags" value="clear" /> 
```

## POI tiers

As a general rule, human NPCs are tougher enemies than zombies.
So, your POI should be _one difficulty tier higher_ than it would be with only zombies in it.

The `khzmusik_Human_Factions_Prefabs_Quests` modlet does not have any tier 1 NPC POI quests,
both for this reason, and because NPC quests are not supposed to show up in the early game.

The difficulty tier is set by this tag in the prefab XML:
```xml
<property name="DifficultyTier" value="2" />
```

## Distributing your POI

You **do not** need my permission to use this modlet to design your POIs.

Your POIs do not need to be distributed as part of this modlet.
If you created a POI that you like, you can ask that it be distributed with this modlet;
I'm always looking for help.
But you don't need to wait for me.

Conversely, I will **not** distribute POIs created by other people, as part of this modlet,
without their express permission.
POI creators can rescind this permission at any time.
If you created a POI that is in this modlet, and do not want me to distribute it,
then contact me, and I will remove the POI.

If you are distributing your POI(s) as part of a modlet, you should probably include this modlet.
(Otherwise, the game won't recognize the sleeper volumes; see the main [README](./README.md).)
You **do not** need my permission to do so (though credit is always appreciated).

However, you _should_ make it very clear to users that you are distributing it.
If they install your modlet, and separately install this one, the two copies will conflict.
