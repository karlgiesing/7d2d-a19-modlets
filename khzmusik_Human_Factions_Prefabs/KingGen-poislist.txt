# POIs from Human Faction Prefabs - for KingGen.
# This is NOT a complete poislist.txt file!
# Add these lines after the @pois tag in another KingGen poislist.txt (vanilla, CompoPack, etc).
# Values are:
# <name>,<x size>,<y size>,<z size>,<y offset>,<rotation to face north>,<allowed biomes>,<allowed townships>,<zones>,<min>,<max>
bandits_faction_settlement_T5,39,19,39,-1,2,wasteland/burnt_forest/desert,wilderness/rural,any,1,1
bandits_faction_site_T2,25,20,25,-4,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness,nozone,1,4
bandits_faction_site_T3,25,16,25,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness,nozone,1,3
bandits_faction_stripclub_T4,33,17,51,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,commercial/downtown,1,2
duke_faction_governer_T4,32,24,50,-4,3,burnt_forest/snow/pine_forest/desert/wasteland,city/town,downtown,1,2
duke_faction_house_modern_T2,28,21,43,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,residentialnew,1,4
duke_faction_house_modern_T3,36,19,46,-5,2,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,residentialnew,1,3
duke_faction_house_modern_T5,45,20,72,-1,2,pine_forest/desert,city/town/rural,residentialold/residentialnew,1,1
military_faction_camp_T3,53,23,57,-3,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness,industrial,1,3
military_faction_police_station_T4,42,25,38,-5,1,burnt_forest/snow/pine_forest/desert/wasteland,city/town,downtown,1,2
military_faction_prison_T5,38,21,49,-1,2,snow/burnt_forest/desert,city/town,downtown,1,1
military_faction_site_T2,25,21,25,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness,nozone,1,5
vault_faction_bombshelter_T3,27,18,37,-10,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness/town/rural,residentialold,1,3
vault_faction_bombshelter_T4,29,23,34,-12,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness/town/rural,residentialold,1,2
vault_faction_bunker_T2,7,15,25,-6,0,burnt_forest/snow/pine_forest/desert/wasteland,,none,1,4
vault_faction_park_T5,40,25,45,-15,0,wasteland/burnt_forest/desert,city/wilderness/town/rural,residentialold/resdentialold,1,1
whisperers_faction_church_T5,44,69,46,-23,2,snow/pine_forest/desert,town/rural,residentialold/downtown/resdentialold,1,1
whisperers_faction_church_graveyard_T2,54,30,60,-2,2,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,residentialold,1,4
whisperers_faction_oldwest_T4,36,13,39,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness/rural,industrial,1,2
whisperers_faction_site_T3,37,42,36,-2,2,burnt_forest/snow/pine_forest/desert/wasteland,wilderness,nozone,1,3
whiteriver_faction_house_T2,24,22,28,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,residentialold,1,4
whiteriver_faction_house_old_tudor_T4,29,27,46,-4,0,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,residentialold,1,2
whiteriver_faction_stadium_T5,139,44,122,-1,2,pine_forest/desert,city/town,any,1,1
whiteriver_faction_trailer_park_T3,47,21,46,-1,2,burnt_forest/snow/pine_forest/desert/wasteland,city/wilderness/town/rural,residentialold,1,3