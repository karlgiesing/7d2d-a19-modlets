# Auto Mechanics

Creates auto mechanic traders and workstations.

## Features

### New motor vehicle workstation

This modlet adds a new workstation, which is required to craft motor vehicles and their parts.
The workstation is not needed for bicycles.

This workstation cannot be crafted.
It is only available at the new auto mechanic trader.

### New auto mechanic trader

This modlet also adds a new auto mechanic trader.
This trader has specialized loot, consisting only of things needed to craft motor vehicles,
and sells only schematics related to motor vehicles.

The trader still gives the traditional quests, and offers the same rewards as 7D2D traders upon
completing these quests.

This trader is the only place that you can find the new motor vehicle workstation,
and so is the only place where players can craft motor vehicles.

As of version 1.0.3, the trader will _not_ be targeted for trader quests,
such as the "Opening Trade Routes" or "White River Citizen" quests.
This is intentional, since the auto mechanic has a limited stock.
If this is undesired, see the Technical Details section for instructions to change it.

## Technical Details

This modlet uses XPath to modify XML files, and does not require SDX or DMT.
It should be compatible with EAC.
Servers should automatically push the XML modifications to their clients, so separate client
installation should not be necessary.

### Targeting the auto mechanic with trader quests

To make the trader available to trader quests,
you must edit the prefab's XML file in the `Prefabs` directory of this modlet.

Remove the comments around this line:
```xml
<!-- <property name="QuestTags" value="trader" /> -->
```
...so it looks like this:
```xml
<property name="QuestTags" value="trader" />
```

You should create a new world after making this change.

### Known Issues

All custom traders (not just this one) will spam the console with warnings if you are using DMS
(Dynamic Music System).
They are warnings, not errors, so they will not affect gameplay,
but they will generate huge log files.
The logging is done in the vanilla C# code, so the issue cannot be solved without code fixes.

There are two ways to deal with it:

* As a workaround, turn off DMS.
* As a fix, install the `0-SphereIICore` modlet. It includes a C# patch that solves the issue.

### Adding the auto mechanic to vanilla RWG maps

The trader is already integrated with vanilla RWG.
It is spawned as a trader, but can spawn half as close to each other as other traders,
which should increase the likelihood of spawning.

The trader is placed in the world when that world is generated.
You will need to create a new world after installing this modlet.

### Adding the auto mechanic to Navezgane

Unfortunately, there is no way to automatically place POIs in Navezgane via XML.
If you are playing on Navezgane, you will have to place this trader manually, after the world is
already generated.

### Adding the auto mechanic to Nitrogen maps

There is a file named `prefablist_khzmusik_AutoMechanics.txt` in the root of this modlet folder.
This is an addition to the prefab list which will tell Nitrogen how to spawn the auto mechanic.

To use it in Nitrogen, you will need to add the file contents to the end of whichever prefab list
you are currently using in Nitrogen.
For example, if you are using it with the Compo Pack (currently at version 47), add the contents
to the end of the `prefablist_CompoPack_47.txt` file.

By default, the auto mechanic trader is spawned in Nitrogen's "trader", "industrial",
"hillbillyjunk", and "carlot" placement zones.
This is because this trader is the only place where you can craft vehicles.
You may add or remove whichever Nitrogen placement zones that you want, by removing/adding the
placement zones at the end of the line in the text file (semicolon-delimited).

As with 7D2D RWG, the trader is placed in the world when that world is generated.
You will need to create a new world after installing this modlet.

### Adding the auto mechanic to KingGen maps

There is a file named `KingGen-poislist.txt` in the root of this modlet folder.
That file contains text to _append to an existing_ `poislist.txt` file,
used by the KingGen random world generator tool.

It is **not** a complete `poislist.txt` file!
Do not try to load it into KingGen as is.
It contains _only_ the data for the new prefabs, and should be appended after the `@pois` tag.
